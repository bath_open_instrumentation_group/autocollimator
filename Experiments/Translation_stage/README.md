This folder contains the data (Data folder) and analysis Jupyter notebook (Analysis folder) for characterising the angular deviation of a linear translation stage at the bottom of a ThorLabs modular XYZ stage (PT3/M)

## Equipment
A plane mirror mounted onto the translation stage was used as the target reflector

## Method
The angle of the plane mirror was measured at 5 mm intervals over the travel range of the translation stage using the central third of the measurement range of the autocollimator. To reduce measurement noise, each measurement consisted of multiple measurements taken over a 10 second period. The experiment was repeated 3 times, each repeat started from the zero stage position

## Data structure
In this experiment, the peak spot positions measured at each stage position were saved with the autocollimator settings (calibration factor and zero position). These peak spot positions were converted into angle in analysis by applying the autocollimator calibration factor. The value of the brightest pixel in each captured frame was also saved. Furthermore, the camera settings (shutter speed) and tracking settings (tracking threshold) used to perform this experiment were saved.

The 'raw_translation_stage_characterisation_data_20210909_165131.txt' and the 'processed_translation_stage_characterisation_data_20210909_165131.txt' data files were obtained from the same experiment. The raw data file contains all measurements taken over each 10 second period. In the processed data file, the raw data taken at each stage position were summarised by averaging the spot positions and finding the maximum of the recorded pixel values.

The separation between the plane mirror and the autocollimator maximised when the translation stage was at zero position. This maximum working distance was measured 5 time by hovering a ruler above the experimental setup. The raw measurements and their average were saved in the 'maximum_seperation_20210909_165131.xlsx' spreadsheet. The averaged result was also saved in the '*translation_stage_characterisation_data_20210909_165131.txt' data files.