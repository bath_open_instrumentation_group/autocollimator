This folder contains the data (Data folder) and analysis Jupyter notebook (Analysis folder) for characterising the thermal drift of the autocollimator

## Equipment
A ThorLabs corner cube (PS975-A) was fixed to the same breadboard as the autocollimator and used as the target reflector to isolate autocollimator drift. The laboratory temperature was monitored using an Adafruit temperature humidity sensor (SHT31-D)

## Method
While leaving the experimental setup untouched in the laboratory, a time series of peak spot position and ambient temperature was recorded. These peak spot positions were converted into angle in analysis by applying the autocollimator calibration factor. The value of the brightest pixel in each captured frame was also saved. Furthermore, the camera settings (shutter speed) and tracking settings (tracking threshold) used to perform the experiment were saved.

## Data structure
Filename: autocollimator_drift_data_20210809_180620.txt
Time period: 18 hours
Conditions: Air conditioner switched on

Filename: autocollimator_drift_data_20210810_133916.txt [Dataset discussed in the paper]
Time period: 115 hours
Conditions: Air conditioner switched on + Ambient

Filename: autocollimator_drift_data_20210819_145452.txt
Time period: 35 hours
Conditions: Ambient

Filename: autocollimator_drift_data_20210906_174143.txt
Time period: 2 hours
Conditions: Ambient

Filename: autocollimator_drift_data_20220314_172858.txt
Time period: 75 hours
Conditions: Air conditioner switched on + polystyrene box