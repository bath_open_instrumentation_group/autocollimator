This folder contains the data and analysis Jupyter notebook for calibration and measurement uncertainty characterisation. The calibration and the measurement uncertainty characterisation experiments used the same equipment and followed the same procedure that generated data with the same structure. The working distance was measure for all experiments, and the autocollimator zero position was measured for some experiments. The data collected as part of the same experiment share the same date

## Equipment
A plane mirror embedded in a ThorLabs POLARIS-K1 mirror mount was used as the target reflector. The angle of the mirror mount was adjusted by rotating a hex key into a mechanical stop

## Method
The angle of the mirror was varied across the measurement range of the autocollimator in steps equivalent to one-sixth of a turn of the adjuster knob. At each step, the peak spot position was monitored for 10 seconds. This procedure was repeated 3 times in yaw (horizontally swept the light spot across the centre of the camera sensor in the direction that increased camera x), and 3 times in pitch (vertically swept the light spot across the centre of the camera sensor in the direction that increased camera y)

## Data structure
The peak spot position and the value of the brightest pixel in each captured frame were saved. The camera settings (shutter speed) and tracking settings (tracking threshold) used to perform each experiment were also saved

Each calibration/measurement uncertainty characterisation experiment generated a ‘raw_data_[date].txt’ file and a ‘processed_data_[date].txt’ file. The raw data file contains all measurements taken over each 10 second period. In the processed data file, the raw data taken at each mirror angle were summarised by averaging the spot positions and finding the maximum of the recorded pixel values. The processed data were saved as a function of the cumulative change in mirror angle. For improved readability, the yaw and pitch data were divided by repeats using subheadings

The separation between the plane mirror and the autocollimator was measured by hovering a ruler above the experimental setup. To minimise measurement error, 5 repeated readings were taken for each working distance measurement. The raw measurements and their average were saved in the 'working_distance_[date].xlsx' spreadsheet

## Zero position measurement
## Equipment
A corner cube (ThorLabs PS975-A) mounted onto a translation stage (ThorLabs PT3/M) was used as the target reflector to displace the laser beam

## Method
The zero x position was found by measuring the peak spot position with the laser beam displaced to the right and left edges of the collimating lens. The zero y position was found by measuring the peak spot position with the laser beam displaced to the top and bottom edges of the collimating lens. Each peak spot position measurement consisted of multiple measurements over a 10 second period

## Data structure
Each zero position measurement experiment generated a ‘raw_zero_data_[date].txt’ file and a ‘processed_zero_data_[date].txt’ file. The raw data file contained all measurements taken over each 10 second period. In the processed data file, the raw data taken at each mirror angle were summarised by averaging the spot positions and finding the maximum of the recorded pixel values. The processed data were saved as a function of the translation stage position