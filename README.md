This is the data archive for the paper

ACute3D: A compact, cost-effective, 3D printed laser autocollimator
by
Qingxin Meng, Julian Stirling, William Wadsworth, and Richard Bowman

## This folder contains the following folders
DesignFiles: Hardware design of the autocollimator provided in the form of parametric CAD files and exported STLs

Software: Software that drives the autocollimator provided in the form of Python scripts

Assembly_instructions: Autocollimator building instructions

Experiments: Data and analysis Jupyter notebook for all performance characterisation experiments carried out in this project

Accessory_code: Code that assisted data analysis

Manuscript: The publication with responses to reviewers comments on the initial submission to the journal

**For more details see the README file in each folder**