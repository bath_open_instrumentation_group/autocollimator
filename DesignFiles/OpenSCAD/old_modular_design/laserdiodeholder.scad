
module reflect(axis){ //reflects its children about the origin, but keeps the originals
	children();
	mirror(axis) children();
}

module cylinder_with_45deg_top(h,r,center=false,extra_height=0.7,$fn=$fn){
	union(){
		rotate([90,0,180]) hull(){
			cylinder(h=h,r=r,$fn=$fn,center=center);
			translate([0,r-0.001,center?0:h/2]) cube([2*sin(45/2)*r,0.002,h],center=true);
		}
		rotate([90,0,180]) translate([0,r-0.001,(center?0:h/2)]) cube([2*sin(45/2)*r,0.002+2*extra_height,h],center=true);
	}
}

module nut_y(d,h=-1,center=false,fudge=1.15,extra_height=0.7,shaft=false,shaft_length=0,top_access=false){ //make a nut, for metric bolt of nominal diameter d
	//d: nominal bolt diameter (e.g. 3 for M3)
	//h: height of nut
	//center: works as for cylinder
	//fudge: multiply the diameter by this number (1.22 works when vertical)
	//shaft: include a long cylinder representing the bolt shaft, diameter=d*1.05
    //top_access: extend the nut upwards to allow it to drop in.
	h=(h<0)?d*0.8:h;
    r=0.9*d*fudge;
    union(){
		rotate([-90,top_access?30:0,0]) cylinder(h=h,center=center,r=r,$fn=6);
		translate([-r*sin(30),center?-h/2:0,0]) cube([2*r*sin(30),h,r*cos(30)+extra_height]);
		if(shaft || shaft_length > 0){
            sl = shaft_length >0 ? shaft_length : 9999;
			translate([0,h/2,0]) reflect([0,1,0]) cylinder_with_45deg_top(h=sl,r=d/2*1.05*fudge,$fn=16,extra_height=extra_height); 
			//the reason I reflect rather than use center=true is that the latter 
			//fails in fast preview mode (I guess because of the lack of points 
			//inside the nut).
		}
        if(top_access){ //hole from the top
            translate([-r*cos(30),center?-h/2:0,0]) cube([2*r*cos(30),h,9999]);
        }
	}
}

d=0.05;
gap_w=2; //size of the gap that is squeezed by the bolt
bolt_span=gap_w+4+d;

difference(){
    union(){
        difference(){
            hull(){
                cylinder(h=12,r=12.6);
                translate([-bolt_span/2,12.6,0]) cube([bolt_span,12,12]);
            }
            cylinder(h=12,r=6);
            cylinder(h=0.5,r2=6,r1=6.5);
            translate([0,16.6,6]) rotate([-90,0,0]){
                translate([bolt_span/2,0,0]) rotate([0,0,-90]) nut_y(4,h=999,shaft=true);
                translate([-bolt_span/2,0,0]) rotate([0,0,90]) cylinder_with_45deg_top(r=4,h=999,$fn=16);
            }
        }
        translate([0,0,12]) difference(){
            cylinder(h=6.5,r=12.6);
            cylinder(h=6.5,r=6);
            translate([0,0,6]) cylinder(h=0.5,r2=6.5,r1=6);
        }
    }
    translate([-1,0,0]) cube([2,24.6,18.5]);
}
