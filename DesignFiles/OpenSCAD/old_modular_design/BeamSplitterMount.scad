length = 45;
width = 24;
fuzz = 1;
large=100;
base_width=32;
base_height=3;
extra_height = 7;
tounge_depth = .8;
tounge_width = 3;
length_at_centre = length-width/2;
trapsize=10;
screw_depth = 10;


difference(){
    
    union(){
        //Main body
        translate([-(width/2+extra_height),-width/2,0])
        {
            cube([width+extra_height,width,length+fuzz],center=false);
        }
        //base
        translate([-(width/2+extra_height),-base_width/2,0])
        {
            cube([base_height,base_width,length+base_width],center=false);
        }
        //alignment "tounge"
        translate([-(width/2+extra_height),0,length/2]){
            cube([2*tounge_depth,tounge_width,length],center=true);
        }
        translate([0,width/2-3,0])
        {
            light_trap(trapheight=length-trapsize/2);
        }
    }
    //Cut at 45
    translate([0,0,length_at_centre]){rotate([45,0,0]){translate([-5*width,-5*width,0])
    {
        cube([10*width,10*width,large],center=false);
    }}}
    //main bore
    cylinder(d=20,h=2*length,centre=true);
    //beam hole (to light trap)
    translate([0,0,length_at_centre])
    {
        rotate([90,0,0]){cylinder(d=5,h=width+fuzz,center=true);}
    }
    //make this less ugly and code smelly
    translate([9,9,0],center=true){cylinder(d=2.6,h=2*screw_depth);}
    translate([9,-9,0],center=true){cylinder(d=2.6,h=2*screw_depth);}
    translate([-9,9,0],center=true){cylinder(d=2.6,h=2*screw_depth);}
    translate([-9,-9,0],center=true){cylinder(d=2.6,h=2*screw_depth);}
}



module light_trap(trapheight=10)
{
    difference()
    {
        translate([-trapsize/sqrt(2),0,0])
        {
            cube([trapsize*sqrt(2),trapsize*sqrt(2),trapheight]);
        }
        translate([0,0,trapheight/2+fuzz])
        {
            cube([trapsize,2*trapsize,trapheight+2*fuzz],center=true);
        }
    }
    //bottom of light trap should be angled
    translate([-trapsize/sqrt(2),0,0])
    {
        cube([trapsize*sqrt(2),trapsize*sqrt(2),3]);
    }
    translate([-trapsize/sqrt(2),0,trapheight])
    {
        //not quite 45 deg!
        rotate([-50,0,0]){cube([trapsize*sqrt(2),trapsize*sqrt(2),3],center=false);}
    }
}
