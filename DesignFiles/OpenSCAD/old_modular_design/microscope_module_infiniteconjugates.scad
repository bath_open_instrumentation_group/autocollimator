/*

An adapter to fit the OpenFlexure Microscope optics module on the
fibre alignment stage

(c) 2016 Richard Bowman - released under CERN Open Hardware License

*/

rms_mount_hole_spacing = 18;
d=0.05;

module each_rms_mounting_hole(){
    for(a=[0,90,180,270]) rotate(a) translate([1,1,0]*rms_mount_hole_spacing/2) children();
}  

module microscope_to_rms_mount_holes(top_z){
    // Holes to bolt the microscope module to the RMS mount above
    for(a=[0,45]) rotate(a) each_rms_mounting_hole(){
        translate([0,0,top_z-3-d]) cylinder(d=3.3, h=999, $fn=8);
        hull(){
            translate([0,0,top_z-3-5-d]) cylinder(r=3, h=d+5, $fn=16);
            translate([2.5,2.5,top_z-3-5-d-10]) cylinder(r=3, h=d+5, $fn=16);
        }
    }
}

module openflexure_microscope_module(rms_mount_l=15){
    // a cut down optics module that bolts to the RMS mount above
    // this is useful for observing the motion of the stage.
    top_z = 65-35-10;
    outer_r = rms_mount_hole_spacing/sqrt(2)+3;
    inner_r = 18; // this should be small enough to fit inside the RMS thread but
                  // big enough to clear the inner lens mount.
    difference(){
        // Start with an optics module:
        import("optics_module_rms_f50d13_infiniteconjugates.stl");
          
        // Chop off the built-in RMS objective mount
        translate([0,0,top_z]) difference(){
            cylinder(r=999,h=999,$fn=3);
            cylinder(d=inner_r,h=999,center=true);
        }
        
        microscope_to_rms_mount_holes(top_z);
    }
    difference(){
        // Add a mounting flange to the top
        hull(){
            translate([0,0,top_z-3]) cylinder(r=outer_r, h=3);
            translate([0,0,top_z-3-10]) cylinder(r=outer_r-10, h=3);
        }
        cylinder(d=inner_r,h=999,center=true);
        microscope_to_rms_mount_holes(top_z);
        for(a=[0,45]) rotate(a+180) translate([-999,12.4,-999]) cube(9999);
    }
    
}

rms_mount_l = 25;
openflexure_microscope_module(rms_mount_l);
