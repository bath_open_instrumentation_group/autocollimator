d = 0.05; //small distance!
picamera_2_camera_h_sensor = 2; //Height of the sensor above the PCB
picamera_2_camera_mount_height = 4.5;

module reflect(axis){ //reflects its children about the origin, but keeps the originals
	children();
	mirror(axis) children();
}

module sequential_hull(){
	//given a sequence of >2 children, take the convex hull between each pair - a helpful, general extrusion technique.
	for(i=[0:$children-2]){
		hull(){
			children(i);
			children(i+1);
		}
	}
}

module picam2_flex_and_components(cw=8.5+1){
    // A 2D perimeter inside which the flex and components of the camera sit.
    // NB this should fit both v1 and v2 of the module
    // cw is the width of the camera module's casing, nominally 8 or 8.5mm but
    // deliberately printed a bit generous to ensure it fits easily without 
    // damaging the flex.
    
    translate([-cw/2,cw/2-1]) square([cw,13.4-cw/2+1]); //flex (also clears v1 connector)
    translate([-cw/2-2.5,6.7]) square([cw+2.5, 5.4]); //connector
}

module picam1_led(){
    // v1 of the camera module has an LED on board that we should make a cut-out for
    translate([5,10]) square([3.5,2]);
    translate([6,8]) square([3.5,2]);
}

module picam2_cutout(beam_length=15){
    // This module is designed to be subtracted from the bottom of a shape.
    // The z=0 plane should be the print bed.
    // It includes cut-outs for the components on the PCB and also a push-fit hole
    // for the camera module.  This uses flexible "fingers" to grip the camera firmly
    // but gently.  Just push to insert, and wiggle to remove.  You may find popping 
    // off the brown ribbon cable and removing the PCB first helps when extracting
    // the camera module again.
    cw = 8.5 + 1.0; //size of camera box sides (NB deliberately loose fitting)
    ch=2.9; //height of camera box (including foam support)
    camera = [cw,cw,ch]; //size of camera box
    hole_r = 4.3; //size of camera aperture
	union(){
        sequential_hull(){
            //cut-out for camera
            translate([0,0,-d]) cube([cw+0.5,cw+0.5,d],center=true); //wider at bottom
            translate([0,0,0.5]) cube([cw,cw,d],center=true);
            translate([0,0,ch/2]) cube([cw,cw,ch],center=true);
            cylinder(r=hole_r, h=2*picamera_2_camera_mount_height, center=true);
        }
            
        //clearance for the ribbon cable at top of camera
        fh=2.5; // the height of the flex
        mh = picamera_2_camera_mount_height;
        
        dz = mh-fh-0.75; // extra height above the flex for the sloping "roof"
        rw = cw - 2*dz; 
        hull(){
            translate([0,0,-d]) linear_extrude(fh) picam2_flex_and_components(cw);
            translate([0,0,-d]) linear_extrude(fh+dz) offset(-dz) picam2_flex_and_components(cw);
        }
        //clearance for the LED/resistor on v1 of the camera
        hull(){
            translate([0,0,-d]) linear_extrude(fh) picam1_led();
            translate([0,0,-d]) linear_extrude(fh+dz) offset(-dz) picam1_led();
        }
        
        //beam clearance
        cylinder(r=hole_r, h=beam_length);
        
        //chamfered screw holes for mounting
        sx = 21/2; //position of screw holes
        reflect([1,0,0]) translate([sx,0,0]) rotate(60){
            //cylinder(r1=3, r2=0,h=4, center=true); //chamfered bottom
            //deformable_hole_trylinder(1.5/2,2.1/2,h=12, center=true);
            cylinder(r1=3.1, r2=1.1, h=5, $fn=3, center=true);
            cylinder(r=1.1, h=20, $fn=3, center=true);
        }
	}
}

module trylinder(r=1, flat=1, h=d, center=false){
    //Halfway between a cylinder and a triangle.
    //NB the largest cylinder that fits inside it has r=r+f/(2*sqrt(3))
    //One of the sides is parallel with the X axis
    hull() for(a=[0,120,240]) rotate(a)
        translate([0,flat/sqrt(3),0]) cylinder(r=r, h=h, center=center);
}

module trylinder_gripper(inner_r=10, h=6, grip_h=3.5, base_r=-1, t=0.65, solid=false, flare=0.4, squeeze=1){
    // This creates a tapering, distorted hollow cylinder suitable for
    // gripping a small cylindrical (or spherical) object
    // The gripping occurs grip_h above the base, and it flares out
    // again both above and below this.
    // inner_r: radius of the cylinder we're gripping
    // h: overall height of the gripper
    // grip_h: height of the part where the gripper touches the cylinder
    // base_r: radius of the (cylindrical) bottom
    // t: thickness of the walls
    // squeeze: how far the wall must be distorted to fit the cylinder
    // flare: how much larger the top is than the gripping part
    // solid: if true, make a solid outline of the gripper
    $fn=48;
    bottom_r=base_r>0?base_r:inner_r+1+t;
    difference(){
        sequential_hull(){
            translate([0,0,0]) cylinder(r=bottom_r,h=d);
            translate([0,0,grip_h-0.5]) trylinder(r=inner_r-squeeze+t,flat=2.5*squeeze,h=d);
            translate([0,0,grip_h+0.5]) trylinder(r=inner_r-squeeze+t,flat=2.5*squeeze,h=d);
            translate([0,0,h-d]) trylinder(r=inner_r-squeeze+flare+t,flat=2.5*squeeze,h=d);
        }
        if(solid==false) sequential_hull(){
            translate([0,0,-d]) cylinder(r=bottom_r-t,h=d);
            translate([0,0,grip_h-0.5]) trylinder(r=inner_r-squeeze,flat=2.5*squeeze,h=d);
            translate([0,0,grip_h+0.5]) trylinder(r=inner_r-squeeze,flat=2.5*squeeze,h=d);
            translate([0,0,h]) trylinder(r=inner_r-squeeze+flare,flat=2.5*squeeze,h=d);
        }
    }
}

module lighttrap_cylinder(r1,r2,h,ridge=1.5){
    //A "cylinder" made up of christmas-tree-like cones
    //good for trapping light in an optical path
    //r1 is the outer radius of the bottom
    //r2 is the inner radius of the top
    //NB for a straight-sided cylinder, r2==r1-ridge
    n_cones = floor(h/ridge);
    cone_h = h/n_cones;
    
	for(i = [0 : n_cones - 1]){
        p = i/(n_cones - 1);
		translate([0, 0, i * cone_h - d]) 
			cylinder(r1=(1-p)*r1 + p*(r2+ridge),
					r2=(1-p)*(r1-ridge) + p*r2,
					h=cone_h+2*d);
    }
}

module optical_path(lens_aperture_r, lens_z){
    // The cut-out part of a camera mount, consisting of
    // a feathered cylindrical beam path.  Camera mount is now cut out
    // of the camera mount body already.
    union(){
        lighttrap_cylinder(r1=5, r2=lens_aperture_r, h=lens_z+d); //beam path
        translate([0, 0, lens_z]) cylinder(r=lens_aperture_r, h=2*d); //lens
    }
}

lens_f = 47.2; //back focal length, NOT nominal focal length
lens_r = 12.7 / 2 + 0.2;
lens_thickness = 5;
gripper_t = 1;
lens_aperture = lens_r - 1.5;
lens_assembly_base_r = 9;

angle_beamdump = 60; //Above 45 degrees to ensure the unwanted laser beam gets fully reflected into the beamdump
angle_ND = 5; //Tilt the ND filter by a small angle to avoid secondary beam while not introducing unnecessary beam displacement
d_M3 = 5.68 + 0.5; //Cap diameter of M3 screw
d_M4 = 7.22 + 0.5;
d_M3_washer = 5;
l_LD = 72.8; //Length of laser diode casing
d_LD = 11 + 0.5;
l_MS = 76; //Length of microscope slide
w_MS = 26 + 0.5; //Width of microscope slide
t_MS = 1 + 1; //Thickness of microscope slide
d_ND = 25.4 + 0.5; //Diameter of the absorptive ND4 filter
t_ND = 2.8 + 1;
l_CM = 25; //Length of the Raspberry Pi camera PCB
w_CM = 24;

t_wall = 3; //Wall thickness
l_cutout = 2 * lens_assembly_base_r; //Side length of square cutouts
l_NDslot = sqrt(3) * d_ND; //ND filter is enclosed in an equilateral triangle
t_NDslot = t_ND / sin(90 - angle_ND); //Vertical thickness of the tilted ND filter slot

l_lid = l_NDslot + 3 + d_M3_washer + 4 * t_wall; //Diameter of the attachment screw holes on the lid are enlarged to allow room for alignment with the M3 screw holes on the autocollimator body
h_lid = t_NDslot + 3 + d_M3_washer + 4 * t_wall;

l_slipplate = l_CM + 2 * d_M3_washer + 4 * t_wall; //Diamter of the counterbored holes on the slip plate are enlarged to allow for 1 mm of camera position adjustment in all directions about the center
w_slipplate = w_CM + 2 * d_M3_washer + 4 * t_wall;
h_slipplate = 6 + t_wall; //The M2 screws used to secure the Pi camera in place have a 6 mm length

l_beamdump = l_LD / 3 + l_NDslot / 2 + 3 + l_cutout / 2 + d_M3 + 4 * t_wall;
h_beamdump = tan(angle_beamdump) * l_cutout + 2 * t_wall;

trans_x = -l_lid / 2; //Every component of the autocollimator is designed about the ND filter, collimating lens, and camera sensor being centred around the z axis
trans_y = -(cos(angle_ND) * d_ND + t_wall);

l_sensor = l_lid / 2 + (l_NDslot / 2 + 3 + 2 * t_wall) + (l_LD / 3); //Overall length of the autocollimator body
w_sensor = w_slipplate + 2; //Additional 2 mm ensures the front and back surfaces of the slip plate do not extend beyond the corresponding outer surface of the autocollimator body at maximum camera position adjustments

l_lid_protrusion = (w_sensor - t_wall) / cos(angle_ND) - 1.5 * d_ND + 0.5 * t_wall; //Length of the tilted beam part of the lid that protrudes into the ND filter slot
h_NDslot = tan(angle_ND) * (w_sensor + trans_y); //Vertical separation between the center of the tilted equilateral triangle part of the ND filter slot and the center of the slot opening on the outer surface of the autocollimator body
lens_assembly_z = lens_f - h_slipplate - 0.3; //The seperation between the collimating lens and the camera sensor is set 0.3 mm shorter than one focal length of the lens to allow for tolerance

h_ND = lens_assembly_z + picamera_2_camera_h_sensor + lens_thickness + t_NDslot / 2 + t_wall; //Height of the center of the tilted equilateral triangle part of the ND filter slot from z = 0
h_beam = h_ND + 30; //The height of the horizontal light beam emitted by the laser diode is found by trial and error, it needs to be as small as possible, while avoiding the ND filter slot intersecting the microscope slide slot. This allows the working distance of the autocollimator to be maximised
h_sensor = h_beam + l_cutout / 2 + d_M4 + 2 * t_wall; //Overall height of the autocollimator body from z = 0

module counterbore_M3(){
    //For parts that attach to the autocollimator body
    cylinder(d=d_M3, h=999);
    translate([0, 0, -999]) cylinder(d=4.0, h=999);
}

module counterbore_M4(){
    //For mounting the autocollimator body onto optics posts
    cylinder(d=d_M4, h=999);
    translate([0, 0, -999]) cylinder(d=5.0, h=999);
}

module LDslot(){
    //This component has the crosssectional shape of a right-angled triangle with the 2 non-right angled vertices trimmed off for compactness of the overall autocollimator design. The slot with such crosssectional shape fully constraints the position and angle of the laser diode with 3 points contact
    difference(){
        rotate([0, 90, 0]){
            hull(){
                translate([0, -d_LD/sqrt(2), 0]) cylinder(d=0.01, h=999, center=true);
                translate([-(sqrt(2)+1)*d_LD/2, d_LD/2, 0]) cylinder(d=0.01, h=999, center=true);
                translate([(sqrt(2)+1)*d_LD/2, d_LD/2, 0]) cylinder(d=0.01, h=999, center=true);
            }
        }
        translate([0, 0, -(999/2+d_LD/2+1)]) cube([999, 999, 999], center=true);
        translate([0, 0, 999/2+d_LD/2+1]) cube([999, 999, 999], center=true);
    }
}
//LDslot();

module NDslot(){
    //This component has the crosssectional shape of an equilateral triangle with a rectangle merged with one of its sides. The length of the rectangle is the same as the length of the side of the triangle it touches, and the width of the rectangle extends to the outer surface of the autocollimator body to create the slot opening. The 2 edges of the slot which the side of the ND filter makes contact with are tilted by 15 degrees, combined with the similarly tilted tip of the lid component, they push down on the ND filter at the 3 contact points as the lid is firmly attached, ensuring the filter lies parallel against the bottom of the slot at 5 degrees
    h_wall = t_ND / cos(15);
    l_rectangle = (w_sensor - t_wall) / cos(angle_ND) - 1.5 * d_ND + 1;
    rotate([angle_ND, 0, 0]) hull(){
        translate([0, -d_ND, 0]) rotate([-15, 0, 0]) cylinder(d=0.01, h=h_wall, center=true);
        rotate([0, 0, 120]) translate([0, -d_ND, 0]) rotate([-15, 0, 0]) cylinder(d=0.01, h=h_wall, center=true);
        rotate([0, 0, 240]) translate([0, -d_ND, 0]) rotate([-15, 0, 0]) cylinder(d=0.01, h=h_wall, center=true);
        translate([0, l_rectangle, 0]) rotate([0, 0, 120]) translate([0, -d_ND, 0]) cylinder(d=0.01, h=t_ND, center=true);
        translate([0, l_rectangle, 0]) rotate([0, 0, 240]) translate([0, -d_ND, 0]) cylinder(d=0.01, h=t_ND, center=true);
    }
}
//NDslot();

module optics_post_attachment(){
    //4 counterbored M4 holes for attaching the autocollimator body to optics posts, The counterbored holes are separated in multiples of 25 mm for better alignment with the screw holes on the optics table
    x = -(l_cutout / 2 + d_M3 / 2 + 3 / 2 + d_M4 / 2 + 2 * t_wall);
    y = trans_y + t_wall;
    z = h_sensor - d_M4 / 2 - t_wall;
    translate([x, y, z]) rotate([-90, 0, 0]) counterbore_M4();
    translate([x, y, z-3*25]) rotate([-90, 0, 0]) counterbore_M4();
    translate([x+2*25, y, z]) rotate([-90, 0, 0]) counterbore_M4();
    translate([x+2*25, y, z-3*25]) rotate([-90, 0, 0]) counterbore_M4();
}

module beamdump_attachment(){
    //4 M3 screw holes for attaching the beamdump to the autocollimator body
    z = h_sensor - (d_M4 + t_wall);
    translate([-(l_cutout/2+d_M3/2+t_wall), w_sensor+trans_y-d_M3/2-t_wall, z]) cylinder(d=3, h=999);
    translate([-(l_cutout/2+d_M3/2+t_wall), trans_y+d_M3/2+t_wall, z]) cylinder(d=3, h=999);
    translate([l_sensor+trans_x-d_M3/2-t_wall, w_sensor+trans_y-d_M3/2-t_wall, z]) cylinder(d=3, h=999);
    translate([l_sensor+trans_x-d_M3/2-t_wall, trans_y+d_M3/2+t_wall, z]) cylinder(d=3, h=999);
}

module slipplate_attachment(){
    //4 M3 screw holes for attaching the slip plate to the autocollimator body. The width of the slip plate is 2 mm less than the width of the autocollimator body, so the front and back surfaces of the slip plate do not extend beyond the corresponding outer surface of the autocollimator body at maximum camera position adjustments
    x = l_CM / 2 + d_M3_washer / 2 + t_wall;
    z = -999 + (h_sensor - d_M4 - 3 * 25 - 2 * t_wall);
    translate([-x, w_sensor+trans_y-1-d_M3_washer/2-t_wall, z]) cylinder(d=3, h=999);
    translate([-x, trans_y+1+d_M3_washer/2+t_wall, z]) cylinder(d=3, h=999);
    translate([x, w_sensor+trans_y-1-d_M3_washer/2-t_wall, z]) cylinder(d=3, h=999);
    translate([x, trans_y+1+d_M3_washer/2+t_wall, z]) cylinder(d=3, h=999);
}

module lid_attachment(){
    //4 M3 screw holes for attaching the lid to the autocollimator body
    x = l_NDslot / 2 + 3 / 2 + t_wall;
    y = w_MS / 2 + t_wall;
    translate([-x, y, h_ND+h_NDslot-3/2-t_NDslot/2-t_wall]) rotate([-90, 0, 0]) cylinder(d=3, h=999);
    translate([-x, y, h_ND+h_NDslot+3/2+t_NDslot/2+t_wall]) rotate([-90, 0, 0]) cylinder(d=3, h=999);
    translate([x, y, h_ND+h_NDslot-3/2-t_NDslot/2-t_wall]) rotate([-90, 0, 0]) cylinder(d=3, h=999);
    translate([x, y, h_ND+h_NDslot+3/2+t_NDslot/2+t_wall]) rotate([-90, 0, 0]) cylinder(d=3, h=999);
}

module LDslot_screw(){
    //2 M3 screw holes for screws that secure the position and angle of the laser diode. There is 5 mm of thread before the screw presses against the laser diode casing
    y = d_LD / 2 + 5;
    translate([l_sensor+trans_x-d_M3/2-t_wall, y, h_beam]) rotate([-90, 0, 0]){
        cylinder(d=d_M3, h=999);
        translate([0, 0, -d_LD]) cylinder(d=3, h=d_LD);
    }
    translate([l_cutout/2+d_M3/2+4*t_wall, y, h_beam]) rotate([-90, 0, 0]){
        cylinder(d=d_M3, h=999);
        translate([0, 0, -d_LD]) cylinder(d=3, h=d_LD);
    }
}

module body(){
    union(){
        difference(){
            translate([trans_x, trans_y, 0]) cube([l_sensor, w_sensor, h_sensor]);
            optical_path(lens_aperture, lens_assembly_z);
            translate([0, 0, 999/2+lens_assembly_z]) cube([l_cutout, l_cutout, 999], center=true);
            translate([-999/2, 0, h_beam]) cube([999, l_cutout, l_cutout], center=true);
            optics_post_attachment();
            beamdump_attachment();
            slipplate_attachment();
            lid_attachment();
            LDslot_screw();
            translate([999/2, 0, h_beam]) LDslot();
            //A Nylon screw should be used to secure the position of the microscope slide inside the autocollimator body to avoid damaging the slide, threads must be taped before using a Nylon screw. There is 5 mm of thread before the screw presses against the crosssectional surface of the slide
            translate([0, 0, h_beam]) rotate([0, 45, 0]) translate([-(l_cutout/sqrt(2)+1), 0, 0]) union(){
                translate([0, -w_MS/2, -t_MS/2]) cube([999, w_MS, t_MS]);
                translate([l_MS-3/2-3*t_wall, 0, t_MS/2+5]){
                    cylinder(d=d_M3, h=999);
                    translate([0, 0, -(t_MS+5+t_wall)]) cylinder(d=3, h=t_MS+5+t_wall);
                }
            }
            //For best printing quality, the tilted ND filter slot has a flat top across the width where it intersects the vertical square cutout. This design enables the top wall to be printed around the intersecting cutout using only bridges. Since there is already material on both sides of the ND filter slot, bridges can be printed along the x axis at the front and back of the cutout first. When the front and back are level, more bridges can be printed along the y axis on both sides of the cutout in the subsequent layer
            translate([0, 0, h_ND]) union(){
                intersection(){
                    linear_extrude(999){
                        projection(cut=true) NDslot();
                    }
                    translate([-999/2, -l_cutout/2, 0]) cube([999, l_cutout, h_NDslot+t_NDslot/2]);
                }
                NDslot();
            }
        }
        translate([0, 0, lens_assembly_z]){
            trylinder_gripper(inner_r=lens_r, h=picamera_2_camera_h_sensor+1+lens_thickness/2, grip_h=picamera_2_camera_h_sensor+1, base_r=-1, t=gripper_t, solid=false, flare=0.4);
            difference(){
                cylinder(r=lens_aperture+1, h=picamera_2_camera_h_sensor);
                cylinder(r=lens_aperture, h=999, center=true);
            }
        }
    }
}
//body();

module beamdump(){
    //This is an optional component designed to be attached to the top of the autocollimator to trap unwanted laser beam. Other beamdumps that do not attach to the autocollimator also works
    difference(){
        translate([-(l_cutout/2+d_M3+2*t_wall), trans_y, 0]) cube([l_beamdump, w_sensor, h_beamdump]);
        translate([0, 0, t_wall]) union(){
            hull(){
                translate([-l_cutout/2, 0, 0]) rotate([90, 0, 0]) cylinder(d=0.01, h=l_cutout, center=true);
                translate([l_beamdump-(l_cutout/2+2*d_M3+4*t_wall), 0, 0]) rotate([90, 0, 0]) cylinder(d=0.01, h=l_cutout, center=true);
                translate([l_cutout/2, 0, tan(angle_beamdump)*l_cutout]) rotate([90, 0, 0]) cylinder(d=0.01, h=l_cutout, center=true);
                translate([l_beamdump-(l_cutout/2+2*d_M3+4*t_wall), 0, tan(angle_beamdump)*l_cutout]) rotate([90, 0, 0]) cylinder(d=0.01, h=l_cutout, center=true);
            }
            translate([0, 0, -999/2]) cube([l_cutout, l_cutout, 999], center=true);
        }
        translate([-(l_cutout/2+d_M3/2+t_wall), trans_y+d_M3/2+t_wall, t_wall]) counterbore_M3();
        translate([-(l_cutout/2+d_M3/2+t_wall), w_sensor+trans_y-d_M3/2-t_wall, t_wall]) counterbore_M3();
        translate([l_beamdump-(l_cutout/2+1.5*d_M3+3*t_wall), trans_y+d_M3/2+t_wall, t_wall]) counterbore_M3();
        translate([l_beamdump-(l_cutout/2+1.5*d_M3+3*t_wall), w_sensor+trans_y-d_M3/2-t_wall, t_wall]) counterbore_M3();
    }
}
//beamdump();

module slip_plate(){
    //This is a compulsary component designed to allow 1 mm of camera position adjustment in all directions about the center. To temporarily hold the part in place during the adjustment process, a spring washer sandwiched between 2 M3 flat washers should be added to each attachment screw
    x = l_CM / 2 + d_M3_washer / 2 + t_wall;
    difference(){
        translate([-l_slipplate/2, trans_y+1, 0]) cube([l_slipplate, w_slipplate, h_slipplate]);
        picam2_cutout();
        translate([-x, w_sensor+trans_y-1-d_M3_washer/2-t_wall, 0]) cylinder(d=d_M3_washer, h=999);
        translate([-x, trans_y+1+d_M3_washer/2+t_wall, 0]) cylinder(d=d_M3_washer, h=999);
        translate([x, w_sensor+trans_y-1-d_M3_washer/2-t_wall, 0]) cylinder(d=d_M3_washer, h=999);
        translate([x, trans_y+1+d_M3_washer/2+t_wall, 0]) cylinder(d=d_M3_washer, h=999);
    }
}
//slip_plate();

module lid(){
    //This is a compulsory component designed to fix the position and angle of the ND filter inside the autocollimator. The short beam that protrudes into the ND filter slot is tilted by 5 degrees relative to horizontal, same as the angle which the slot is tilted by. The tip of the beam is tilted by 15 degrees relative to vertical, combined with the 2 similarly tilted edges inside the slot, they push down on the ND filter at the 3 contact points as the lid is firmly attached, ensuring the filter lies parallel against the bottom of the slot at 5 degrees
    x = l_NDslot / 2 + 3 / 2 + t_wall;
    z = t_NDslot / 2 + 3 / 2 + t_wall;
    difference(){
        union(){
            rotate([angle_ND, 0, 0]) hull(){
                difference(){
                    union(){
                        translate([-25.4/2, -l_lid_protrusion, 0]) rotate([15, 0, 0]) cylinder(d=0.01, h=999, center=true);
                        translate([25.4/2, -l_lid_protrusion, 0]) rotate([15, 0, 0]) cylinder(d=0.01, h=999, center=true);
                        translate([-25.4/2, 99, 0]) cylinder(d=0.01, h=999, center=true);
                        translate([25.4/2, 99, 0]) cylinder(d=0.01, h=999, center=true);
                    }
                    translate([0, 0, -(999+2.8)/2]) cube([999, 999, 999], center=true);
                    translate([0, 0, (999+2.8)/2]) cube([999, 999, 999], center=true);
                }
            }
            translate([0, t_wall/2, 0]) cube([l_lid, t_wall, h_lid], center=true);
        }
        translate([0, 999/2+t_wall, 0]) cube([999, 999, 999], center=true);
        //Since the amount the tilted beam protrudes into the slot is unknown, the 4 attachment screw holes are enlarged to allow room for alignment with the 4 screw holes on the autocollimator body which are parallel with the y axis
        translate([-x, 0, -z]) rotate([90, 0, 0]) cylinder(d=d_M3_washer, h=999, center=true);
        translate([-x, 0, z]) rotate([90, 0, 0]) cylinder(d=d_M3_washer, h=999, center=true);
        translate([x, 0, -z]) rotate([90, 0, 0]) cylinder(d=d_M3_washer, h=999, center=true);
        translate([x, 0, z]) rotate([90, 0, 0]) cylinder(d=d_M3_washer, h=999, center=true);
    }
}
//lid();

//difference(){
union(){
    translate([0, 0, h_sensor+5]) beamdump();
    translate([0, 0, -(h_slipplate+5)]) slip_plate();
    translate([0, w_sensor+trans_y+l_lid_protrusion+5, h_ND+h_NDslot]) lid();
    body();
}
//    translate([0, -999/2, 0]) cube([999, 999, 999], center=true);
//}

//projection(cut = true) rotate([90, 0, 0]) union(){
//    translate([0, 0, h_sensor+5]) beamdump();
//    translate([0, 0, -(h_slipplate+5)]) slip_plate();
//    translate([0, w_sensor+trans_y+l_lid_protrusion+5, h_ND+h_NDslot]) lid();
//    body();
//}