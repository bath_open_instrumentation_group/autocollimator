shim_h = 0.2;

difference(){
    rotate([0, 0, 45]) intersection(){
        import("optics_module_rms_f50d13_infiniteconjugates.stl");
        translate([0, 0, -20.5]) cylinder(r = 999, h = shim_h);
    }
    translate([0, 10.5, 0]) cylinder(d = 3, h = 999, $fn = 25, center = true);
    translate([0, -10.5, 0]) cylinder(d = 3, h = 999, $fn = 25, center = true);
}
