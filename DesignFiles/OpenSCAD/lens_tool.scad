// This is a trivial tool to support a 16mm lens as it is inserted
// into the holder.  Part of the OpenFlexure Microscope.

// (c) 2017 Richard Bowman, released under CERN open hardware license.

$fn = 32;

h = 80;
d = 12.7;

difference(){
    union(){
        cylinder(d=d-0.2, h=h);
        cylinder(d1=29, d2=d-0.2, h=10);
    }
    // hollow it out
    cylinder(d=d-4.7, h=999, center=true);
    // bevel the top
    translate([0,0,h-4]) cylinder(d1=8, d2=10.5, h=4.01);
}