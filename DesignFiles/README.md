The code used to produce the autocollimator (monolithic body and other components) is provided along with the exported STLs.

The code used to produce a old modular version is also provided in the folder 'OpenSCAD\Old_modular_design' for completeness of the project.

**All files released under the CERN OHL license.**