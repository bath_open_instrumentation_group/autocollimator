"""Add/read EXIF to/from an image. The EXIF contains shutter speed and gain
settings of the camera at the time of image capture"""

import piexif
import piexif.helper
import json

#exif_dict = piexif.load("exif.jpg")
#metadata = {"Shutter_speed": 1.0, "digital_gain": 2.0, "analogue_gain": 3.0}
#metadata_string = json.dumps(metadata, indent = 1)
#exif_dict["Exif"][piexif.ExifIFD.UserComment] = metadata_string.encode()
#exif_bytes = piexif.dump(exif_dict)
#piexif.insert(exif_bytes, "exif.jpg")

exif_dict = piexif.load(r"..\Frames\frame_20210726_122641.jpg")
user_comment = json.loads(exif_dict["Exif"][piexif.ExifIFD.UserComment].decode())
print(user_comment)
