This folder contains the 'ACute3D: A compact, cost-effective, 3D printed laser autocollimator' manuscript.

The 'Proof' folder contains the final proof corrections sent to the editor prior to publication.

The 'TIM_22_00586R1' folder contains the accepted version along with figures, and author biography.

The 'Submissions' folder contains the initial and review versions sent to the journal, along with responses to reviewers comments.