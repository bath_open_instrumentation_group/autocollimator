import os
import cv2
import sys
import time
import math
import json
import argparse
import select
import numpy as np
from numpy import linalg as LA
from scipy import ndimage
from scipy.optimize import leastsq
from copy import copy
from dataclasses import dataclass
import picamerax
from picamerax import PiCamera
from picamerax.array import PiRGBArray
from picamerax import mmal, mmalobj, exc
from picamerax.mmalobj import to_rational
import piexif
import piexif.helper
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def rgb_image(camera, resize = None, **kwargs):
    """Capture an image and return an RGB numpy array"""
    with PiRGBArray(camera, size = resize) as output:
        camera.capture(output, format = 'rgb', resize = resize, use_video_port = True, **kwargs)
        return output.array

def image_peak(image, fraction):
    """Apply a threshold to an image and return the center of the light spot in the image"""
    image += ((image.max() - image.min()) * fraction) - image.max()
    image = cv2.threshold(image, 0, 0, cv2.THRESH_TOZERO)[1]
    peak = ndimage.measurements.center_of_mass(image)
    peak = (peak[1], peak[0])
    return peak, image

def image_boolean(image, peak):
    """Convert an image to a boolean image"""
    image_bool = copy(image)
    xp, yp = np.meshgrid(np.arange(np.shape(image_bool)[1]), np.arange(np.shape(image_bool)[0]))
    xp = xp.astype(np.float64)
    yp = yp.astype(np.float64)
    xp -= peak[0]
    yp -= peak[1]
    image_bool = image_bool.astype(bool)
    return image_bool, xp, yp

def spot_radius(image_bool, xp, yp):
    """Calculate the distance between the spot centre and every high pixel in an image
    and return the maximum distance"""
    radius = np.sqrt((np.abs(xp[image_bool]) + 0.5)**2 + (np.abs(yp[image_bool]) + 0.5)**2)
    r_max = np.max(radius)
    return r_max

def spot_infill(image_bool, r_max):
    """Return the amount of overlap between the light spot and a concentric circle with radius
    equal to the maximum radius of the spot"""
    infill = np.sum(image_bool) / (np.pi * r_max**2)
    return infill

def image_analysis(image, image_bool, xp, yp):
    """Calculate the second moments of the light spot about its principle axes using
    the covariance matrix and return a ratio.
    Return the kurtosis of the light spot, a normal distribution has a kurtosis of 3"""
    total = np.sum(image)
    x2 = np.sum(image[image_bool] * xp[image_bool]**2) / total
    y2 = np.sum(image[image_bool] * yp[image_bool]**2) / total
    xy = np.sum(image[image_bool] * xp[image_bool] * yp[image_bool]) / total
    x4 = np.sum(image[image_bool] * xp[image_bool]**4) / total
    y4 = np.sum(image[image_bool] * yp[image_bool]**4) / total
    x2y2 = np.sum(image[image_bool] * xp[image_bool]**2 * yp[image_bool]**2) / total
    covariance = np.array([[x2, xy], [xy, y2]])
    eigenvalue = LA.eig(covariance)[0]
    if eigenvalue[0] > eigenvalue[1]:
        second_moments_ratio = eigenvalue[1] / eigenvalue[0]
    if eigenvalue[1] > eigenvalue[0]:
        second_moments_ratio = eigenvalue[0] / eigenvalue[1]
    if eigenvalue[0] == eigenvalue[1]:
        second_moments_ratio = 1.0
    second_moment = x2 + y2
    fourth_moment = x4 + y4 + (2.0 * x2y2)
    kurtosis = fourth_moment / (second_moment**2)
    return second_moments_ratio, kurtosis

def camera_settings_adjustment(camera):
    """Adjust and freeze camera settings at startup"""
    print('Freezing the camera settings')
    camera.shutter_speed = camera.exposure_speed
    print('Shutter speed: {}'.format(camera.shutter_speed))
    camera.exposure_mode = 'off'
    print('Auto exposure disabled')
    g = camera.awb_gains
    camera.awb_mode = 'off'
    camera.awb_gains = g
    print('Auto white balance disabled, gains are {}'.format(g))
    camera.brightness = camera.brightness
    print('Brightness: {}'.format(camera.brightness))
    camera.contrast = camera.contrast
    print('Contrast: {}'.format(camera.contrast))
    camera.analog_gain = 1
    camera.digital_gain = 1
    time.sleep(1)
    print('Analogue gain: {}, Digital gain: {}'.format(camera.analog_gain, camera.digital_gain))

    print('Adjusting shutter speed to avoid saturation...')
    image = rgb_image(camera)
    max_pixel_value = np.max(image)
    current_shutter_speed = camera.shutter_speed
    while (max_pixel_value > 230.0) and (current_shutter_speed > 9):
        camera.shutter_speed = math.floor(current_shutter_speed * 230.0 / max_pixel_value)
        time.sleep(0.5)
        image = rgb_image(camera)
        max_pixel_value = np.max(image)
        current_shutter_speed = camera.shutter_speed
    if (current_shutter_speed == 9) and (max_pixel_value == 255.0):
        raise RuntimeError('Signal too bright')
    print(
        'Current shutter speed: {}, maximum pixel value: {}'.
        format(current_shutter_speed, max_pixel_value)
        )

def limit(x):
    """Sets a convergence threshold. Convergence requires the difference between
    adjacent elements to be smaller than this threshold"""
    x = np.abs(x)
    average = np.mean(x)
    digits = int(math.log10(average)) + 1
    y = 10**digits
    return y

def save_camera_settings(camera, output):
    """Save the camera settings to a JSON file"""
    camera_settings = {'analog_gain': float(getattr(camera, 'analog_gain')),
    'digital_gain': float(getattr(camera, 'digital_gain')),
    'shutter_speed': getattr(camera, 'shutter_speed'),
    'awb_gains': (float(getattr(camera, 'awb_gains')[0]), float(getattr(camera, 'awb_gains')[1])),
    'awb_mode': getattr(camera, 'awb_mode'),
    'exposure_mode': getattr(camera, 'exposure_mode'),
    'brightness': getattr(camera, 'brightness'),
    'contrast': getattr(camera, 'contrast')}
    with open(output, 'w') as outfile:
        json.dump(camera_settings, outfile, indent = 1)

def load_camera_settings(camera, inputf):
    """Load camera settings from a JSON file"""
    with open(inputf, 'r') as infile:
        settings = json.load(infile)
        for k, v in settings.items():
            setattr(camera, k, v)

def save_tracking_setting(fraction, output):
    """Save the optimum amount of background to remove in spot tracking to a JSON file"""
    tracking_setting = {'fraction': fraction}
    with open(output, 'w') as outfile:
        json.dump(tracking_setting, outfile, indent = 1)

def load_tracking_setting(inputf):
    """Load the optimum amount of background to remove in spot tracking from a JSON file"""
    with open(inputf, 'r') as infile:
        settings = json.load(infile)
        fraction = settings['fraction']
    return fraction

def create_overlay(camera):
    """create a crosshair overlay"""
    pad = np.zeros((32, 32, 4), dtype = np.uint8)
    pad[:, 15:17, :] = 255
    pad[15:17, :, :] = 255
    pad[8:-8, 8:-8, :] = 0
    overlay = camera.add_overlay(pad.tobytes(), size = (32, 32), layer = 3, fullscreen = False)
    return overlay

def move_overlay(px, py, overlay, camera):
    """Move the overlay to follow the light spot as the peak position is updated"""
    factor_x = camera.preview_window[2] / camera.resolution.width
    factor_y = camera.preview_window[3] / camera.resolution.height
    x = int(round(px * factor_x - 32.0 / 2.0))
    y = int(round(py * factor_y - 32.0 / 2.0 + 35.0))
    overlay.window = (x, y, 32, 32)

def flat_lens_shading_table(camera: PiCamera) -> np.ndarray:
    """Return a flat (i.e. unity gain) lens shading table"""
    if not hasattr(PiCamera, "lens_shading_table"):
        raise ImportError(
            "This program requires the forked picamera library with lens shading support"
        )
    return np.zeros(camera._lens_shading_table_shape(), dtype = np.uint8) + 32
