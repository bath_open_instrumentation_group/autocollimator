import os
import cv2
import sys
import time
import math
import json
import argparse
import select
import numpy as np
from numpy import linalg as LA
from scipy import ndimage
from scipy.optimize import leastsq
from copy import copy
from dataclasses import dataclass
import picamerax
from picamerax import PiCamera
from picamerax.array import PiRGBArray
from picamerax import mmal, mmalobj, exc
from picamerax.mmalobj import to_rational
import piexif
import piexif.helper
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import spot_tracking
import autocollimator as device

def addparser(parser):
    """Add parsers to the program"""
    parser.add_argument(
        '-cpo', '--camera_preview_off', action = 'store_true',
        help = 'Turn off camera preview after startup'
        )
    parser.add_argument(
        '-sdo', '--save_data_off', action = 'store_true',
        help = 'Do not save measurement data'
        )
    parser.add_argument(
        '-sf', '--save_frame', action = 'store_true',
        help = 'Save every frame captured during measurement'
        )
    parser.add_argument(
        '-p', '--pixel', action = 'store_true',
        help = 'Display the peak position in pixels'
        )
    parser.add_argument(
        '-rcs', '--reset_camera_settings', action = 'store_true',
        help = 'Generate new camera settings'
        )
    parser.add_argument(
        '-rts', '--reset_tracking_setting', action = 'store_true',
        help = 'Generate new tracking threshold'
        )
    parser.add_argument(
        '-rc', '--reset_calibration', action = 'store_true',
        help = 'Perform a new calibration'
        )

def setup_camera(args, camera):
    """Load or generate camera settings"""
    if (not args.reset_camera_settings) & (
        os.path.isfile(os.path.join(args.directory, 'Settings', 'camera_settings.json'))
        ):
        spot_tracking.load_camera_settings(
            camera, os.path.join(args.directory, 'Settings', 'camera_settings.json')
            )
        time.sleep(1)
        print('Loaded camera settings, may reduce exposure if needed to avoid pixel saturation')
    spot_tracking.camera_settings_adjustment(camera)
    spot_tracking.save_camera_settings(
        camera, os.path.join(args.directory, 'Settings', 'camera_settings.json')
        )

def setup_tracking(args, camera):
    """Load or generate tracking setting"""
    if (not args.reset_tracking_setting) & (
        os.path.isfile(os.path.join(args.directory, 'Settings', 'tracking_setting.json'))
        ):
        fraction = spot_tracking.load_tracking_setting(
            os.path.join(args.directory, 'Settings', 'tracking_setting.json')
            )
        print('Loaded tracking threshold: {:.2f}'.format(fraction))
    else:
        fraction = device.tracking_setting_adjustment(camera)
        spot_tracking.save_tracking_setting(
            fraction, os.path.join(args.directory, 'Settings', 'tracking_setting.json')
            )
    return fraction

def setup_calibration(args):
    """Load calibration coefficients or initiate the interactive calibration procedure"""
    if args.pixel:
        calibration = None
    elif (not args.pixel) & (not args.reset_calibration) & (
        os.path.isfile(os.path.join(args.directory, 'Settings', 'calibration_settings.json'))
        ):
        calibration = device.load_calibration_settings(
            os.path.join(args.directory, 'Settings', 'calibration_settings.json')
            )
    else:
        calibration = None
        args.reset_calibration = True
        print('Performing calibration')
    return calibration, args

def startup(preview_w, preview_h, args, camera):
    """Initiate the program"""
    print('Starting up the camera and letting it auto-expose')
    camera.start_preview(window = (0, 35, preview_w, preview_h), fullscreen = False)
    time.sleep(3)
    camera.lens_shading_table = spot_tracking.flat_lens_shading_table(camera)
    time.sleep(1)
    setup_camera(args, camera)
    fraction = setup_tracking(args, camera)
    calibration, args = setup_calibration(args)
    return fraction, calibration, args

def save_labelled_image(image, args, camera):
    """Label image with camera settings and save"""
    filename = 'frame_%s.jpg' % time.strftime("%Y%m%d_%H%M%S")
    cv2.imwrite(os.path.join(args.directory, 'Frames', filename), image)
    exif_dict = piexif.load(os.path.join(args.directory, 'Frames', filename))
    metadata = {'Shutter speed': getattr(camera, 'shutter_speed'),
    'Analogue gain': float(getattr(camera, 'analog_gain')),
    'Digital gain': float(getattr(camera, 'digital_gain'))}
    metadata_string = json.dumps(metadata, indent = 1)
    exif_dict["Exif"][piexif.ExifIFD.UserComment] = metadata_string.encode()
    exif_bytes = piexif.dump(exif_dict)
    piexif.insert(exif_bytes, os.path.join(args.directory, 'Frames', filename))

def averaged_data_acquisition(fraction, overlay, camera):
    """Measure for 10 seconds to collect data needed to calculate an average.
    The measurement is stopped if no signal is detected"""
    save_data = True
    data = []
    start_t = time.time()
    current_t = time.time()
    while current_t - start_t < 10.0:
        image, max_pixel_value = device.image_green(camera)
        if max_pixel_value == 0.0:
            print('NO SIGNAL DETECTED, MAKE SURE THE LIGHT SPOT IS IN THE FIELD OF VIEW')
            image = None
            data = None
            save_data = False
            break
        else:
            peak = spot_tracking.image_peak(image, fraction)[0]
            if overlay != None:
                spot_tracking.move_overlay(*peak, overlay, camera)
            data.append([*peak, max_pixel_value])
            current_t = time.time()
    return image, data, save_data

def averaged_data_processing(image, data, settings, args, camera, output):
    """Convert an input array of peak spot positions to the specified output type.
    If the output is set to angle, print the mean and standard deviation of yaw and pitch,
    then return the mean peak position.
    If the output is set to pixel, print the mean and standard deviation of
    the peak spot position.
    If the output is set to displacement, return the mean displacement.
    Prints a warning if the signal intensity is too weak or too intense
    during data acquisition"""
    data_arr = np.array(data)
    save_labelled_image(image, args, camera)

    pixel_value_max = np.max(data_arr[:, 2], axis = 0)
    if 0.0 < pixel_value_max < 30.0:
        print('THE SIGNAL IS TOO WEAK, CONSIDER INCREASING CAMERA SHUTTER SPEED')
    elif 240.0 < pixel_value_max < 255.0:
        print('THE SIGNAL IS TOO INTENSE, CONSIDER REDUCING CAMERA SHUTTER SPEED')
    elif pixel_value_max == 255.0:
        print('DETECTED PIXEL SATURATION, REDUCE CAMERA SHUTTER SPEED')

    peak_mean = np.mean(data_arr[:, :2], axis = 0)
    peak_std = np.std(data_arr[:, :2], ddof = 1, axis = 0)

    if output == 'angle':
        yaw, pitch = device.calculate_angle(data_arr[:, 0], data_arr[:, 1], settings)
        yaw_mean = np.mean(yaw)
        yaw_std = np.std(yaw, ddof = 1)
        pitch_mean = np.mean(pitch)
        pitch_std = np.std(pitch, ddof = 1)
        print(
            'Measured angle (arcsec): ({:.2f}, {:.2f})   STD: ({:.2g}, {:.2g})'
            .format(yaw_mean, pitch_mean, yaw_std, pitch_std)
            )
        return peak_mean, peak_std, pixel_value_max
    elif output == 'displacement':
        displacement = device.calculate_displacement(data_arr[:, 0], settings, camera)
        displacement_mean = np.mean(displacement)
        displacement_std = np.std(displacement, ddof = 1)
        return displacement_mean, displacement_std, peak_mean, peak_std, pixel_value_max
    elif output == 'pixel':
        print(
            'Peak position (px): ({:.2f}, {:.2f})   STD: ({:.2g}, {:.2g})'
            .format(*peak_mean, *peak_std)
            )
        return peak_mean, peak_std, pixel_value_max

def write_to_output_files(message, processed_outfile, raw_outfile, print_message = False):
    """Write the same message to 2 files, with the option to print the message to the terminal"""
    for file in [processed_outfile, raw_outfile]:
        file.write(message)
    if print_message:
        print(message)

def preview(args, camera):
    """Create a crosshair overlay if the user does not instruct the camera preview
    to be turned off"""
    if args.camera_preview_off:
        camera.stop_preview()
        overlay = None
    else:
        overlay = spot_tracking.create_overlay(camera)
    return overlay

def terminate_continuous_measurement(outfile, args, camera):
    """Ensures the camera preview is turned off and the data file is closed properly"""
    time.sleep(0.5)
    print('\nGot a keyboard interrupt, stopping')
    if not args.save_data_off:
        outfile.close()
        print(
            'The data file has been saved under the directory:\n{}'
            .format(os.path.join(args.directory, 'Data'))
            )
    if not args.camera_preview_off:
        camera.stop_preview()

def acquire_linear_measurement(
    min_range, max_range, step, repeat, processed_outfile, raw_outfile,
    fraction, settings, overlay, args, camera, output
    ):
    """Increment over a measurement range with a contant interval,
    taking measurements at each step"""
    positions = np.arange(min_range, int(max_range + 1), step)
    n = np.shape(positions)[0]
    m = int(repeat + 1)
    tot_cal_points = n * m
    x_mean = np.zeros([m, n])
    y_mean = np.zeros([m, n])
    processed_outfile.write(
        'Position (mm), Peak x position (px), Peak y position (px), Maximum pixel value\n'
        )
    raw_outfile.write('Peak x position (px), Peak y position (px), Maximum pixel value\n')
    for i in range(m):
        if i > 0:
            write_to_output_files(
                'Repeat No. {}\n'.format(i),
                processed_outfile, raw_outfile, print_message = True
                )
        for j in range(n):
            save_data = False
            while not save_data:
                input("Move target to {} mm and then press 'ENTER'".format(positions[j]))
                print('Taking measurements...')
                frame, data, save_data = averaged_data_acquisition(
                    fraction, overlay, camera
                    )
            peak_mean, peak_std, pixel_value_max = averaged_data_processing(
                frame, data, settings, args, camera, output
                )
            x_mean[i, j] = peak_mean[0]
            y_mean[i, j] = peak_mean[1]
            processed_outfile.write(
                '{}, {}, {}, {}\n'.format(positions[j], *peak_mean, pixel_value_max)
                )
            raw_outfile.write(str(data) + '\n')
            print(
                'Collected {} of {} calibration points'.format(i * n + j + 1, tot_cal_points)
                )
    if output == 'pixel':
        return positions, x_mean, y_mean

def printProgressBar(iteration, total, length = 10):
    """A terminal progress bar"""
    percent = 100.0 * iteration / total
    filledLength = int(length * iteration // total)
    bar = '*' * filledLength + '-' * (length - filledLength)
    print('Progress: |%s| %d%% Completed' % (bar, percent), end = '\r')
    if iteration == total: 
        print('')

def input_nonblocking():
    """Return a key if the operator has pressed one, or None"""
    fd = select.select([sys.stdin], [], [], 0)
    if fd == ([], [], []):
        return None
    else:
        return input()
