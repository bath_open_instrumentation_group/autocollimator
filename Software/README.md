This folder contains the software that drives the autocollimator. The data collection code for all experiments performed in this project are embedded in the software.

The top-level file is 'autocollimator.py'. The functions in 'shared_functions.py' and 'spot_tracking.py' are shared with an open source distance sensor also developed by the Bath Open Instrumentation Group.

## Environment
Raspberry Pi 3

Python 3

Run the following commands in the Raspberry Pi terminal to install necessary packages:

python3 -m pip install pip --upgrade

sudo apt-get install libatlas-base-dev

python3 -m pip install numpy --upgrade

python3 -m pip install scipy

python3 -m pip install opencv-python

python3 -m pip install picamerax

python3 -m pip install piexif

python3 -m pip install matplotlib

python3 -m pip install adafruit-circuitpython-sht31d

See requirements.txt for details on the version of all packages we had installed.

In the Raspberry Pi file manager, choose an autocollimator directory, by default this is: '/home/pi/autocollimator'.

Create 4 folders under this directory: 'Code', 'Data', 'Frames', 'Settings'. Then copy and paste the software Python scripts into the 'Code' folder.

## Running the software
In the Raspberry Pi terminal, navigate to the 'Code' folder under the autocollimator directory and run the following command to start the software:

python3 autocollimator.py

Running the software for the first time automatically initiates the interactive calibration process, follow the instructions printed in the terminal to perform spot position to target angle calibration. We assume you are calibrating against a ThorLabs Polaris K1 mirror mount by turning a hex key into a mechanical stop. A corner cube (ThorLabs PS975-A) is also needed to measure the zero position of the autocollimator.

## The software can take the following arguments
'-p', '--pixel': Display the peak spot position in pixels rather than the default measured angle. This argument can be used to run the software without calibration.

'-rcs', '--reset_camera_settings': Generate new camera settings. All settings files can be found in the 'Settings' folder.

'-rts', '--reset_tracking_setting': Generate new tracking setting. All settings files can be found in the 'Settings' folder.

'-rc', '--reset_calibration': Perform a new calibration. All settings files can be found in the 'Settings' folder.

'-sf', '--save_frame': Save every frame captured during operation to the 'Frames' folder. Outside the calibration procedure, the software does not save captured frames by default.

'-sdo', '--save_data_off': Do not save any data generated during operation. All measurement data are saved to the 'Data' folder by default.

'-cpo', '--camera_preview_off': Turn off camera preview after startup. The camera preview stays on during operation by default.

'-ts', '--translation_stage': The code for the linear translation stage angular deviation characterisation experiment discussed in the paper. This argument can be used to replicate our experiment.

'-d', '--directory': Specify a path to the autocollimator directory. By default the path is '/home/pi/autocollimator'.

## For example
In the Raspberry Pi terminal, running the following command:

python3 autocollimator.py -p -rcs -rts

Resets the camera and spot tracking settings, and displays the peak position of the laser spot in pixels.

**All files released under the GNU GPL v3 license.**