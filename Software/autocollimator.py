#! /usr/bin/env python3
import os
import cv2
import sys
import time
import math
import json
import argparse
import select
import numpy as np
from numpy import linalg as LA
from scipy import ndimage
from scipy.optimize import leastsq
from copy import copy
from dataclasses import dataclass
import picamerax
from picamerax import PiCamera
from picamerax.array import PiRGBArray
from picamerax import mmal, mmalobj, exc
from picamerax.mmalobj import to_rational
import piexif
import piexif.helper
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import busio
import board
import adafruit_sht31d
import spot_tracking
import shared_functions

# We assume you are using a ThorLabs Polaris 1" mirror mount
STEP_SIZE = (7.0 / 6.0) * 10**(-3) * 180.0 * 3600.0 / np.pi

def image_green(camera):
    """Capture an image and return the green channel of this image"""
    image = spot_tracking.rgb_image(camera)
    max_pixel_value = np.max(image)
    image = image[:, :, 1]
    image = image.astype(np.float32)
    return image, max_pixel_value

def tracking_setting_adjustment(camera):
    """Return the optimum amount of background to remove from an image for image tracking at startup"""
    print('Finding the optimum amount of background to remove for spot tracking')
    fraction = np.linspace(1.0, 0.4, 61).round(2)
    n = np.shape(fraction)[0]
    centre = np.zeros((2, n))
    r_max = np.zeros(n)
    infill = np.zeros(n)
    second_moments_ratio = np.zeros(n)
    kurtosis = np.zeros(n)
    image = image_green(camera)[0]

    for i in range(n):
        image_copy = copy(image)
        peak, image_copy = spot_tracking.image_peak(image_copy, fraction[i])
        centre[:, i] = peak
        image_bool, xp, yp = spot_tracking.image_boolean(image_copy, peak)
        radius = spot_tracking.spot_radius(image_bool, xp, yp)
        r_max[i] = radius
        infill[i] = spot_tracking.spot_infill(image_bool, radius)
        second_moments_ratio[i], kurtosis[i] = spot_tracking.image_analysis(image_copy, image_bool, xp, yp)
        shared_functions.printProgressBar(i, n - 1)
    gradient_r_max = np.gradient(r_max, 0.01)
    limit_r_max = spot_tracking.limit(gradient_r_max)
    gradient_infill = np.gradient(infill, 0.01)
    limit_infill = spot_tracking.limit(gradient_infill)
    gradient_kurtosis = np.gradient(kurtosis, 0.01)
    limit_kurtosis = spot_tracking.limit(gradient_kurtosis)

    F = []
    for i in range(n):
        if (i < n - 1) and (r_max[i] < 10.0):
            F.append(fraction[i])
            break
        elif i == n - 1:
            raise RuntimeError('Spot tracking failure, check the camera sensor '
            'placement relative to the collimating lens is exactly one focal length')
    for i in range(n):
        if (i < n - 1) and (infill[i] > 0.5):
            F.append(fraction[i])
            break
        elif i == n - 1:
            print('Spot quality warning, infill percentage > 0.5 not satisfied')
    for i in range(n):
        if (i < n - 1) and (second_moments_ratio[i] > 0.5):
            F.append(fraction[i])
            break
        elif i == n - 1:
            print('Spot quality warning, second moments ratio > 0.5 not satisfied')
    for i in range(n):
        if (i < n - 1) and (gradient_r_max[i:] < limit_r_max).all():
            F.append(fraction[i])
            break
        elif i == n - 1:
            print('Spot quality warning, maximum radius has not stabilised')
    for i in range(n):
        if (i < n - 1) and (gradient_infill[i:] < limit_infill).all():
            F.append(fraction[i])
            break
        elif i == n - 1:
            print('Spot quality warning, infill percentage has not stabilised')
    for i in range(n):
        if (i < n - 1) and (gradient_kurtosis[i:] < limit_kurtosis).all():
            F.append(fraction[i])
            break
        elif i == n - 1:
            print('Spot quality warning, kurtosis has not stabilised')
    F = np.array(F)
    fraction = (np.min(F) - 0.1).round(2)

    f = np.linspace(fraction - 0.1, fraction + 0.1, 21).round(2)
    n = np.shape(f)[0]
    centre = np.zeros((2, n))
    for i in range(n):
        image_copy = copy(image)
        centre[:, i] = spot_tracking.image_peak(image_copy, f[i])[0]
    x_range = np.max(centre[0, :]) - np.min(centre[0, :])
    y_range = np.max(centre[1, :]) - np.min(centre[1, :])
    x_mean = np.mean(centre[0, :])
    y_mean = np.mean(centre[1, :])
    x_uncertainty = (x_range / 2.0) / x_mean
    y_uncertainty = (y_range / 2.0) / y_mean
    uncertainty = np.sqrt(x_uncertainty**2 + y_uncertainty**2) * 100.0

    print('Current tracking threshold: {:.2f}'.format(fraction))
    print('Percentage uncertainty in spot position: {:.2g}%'.format(uncertainty))
    return fraction

@dataclass
class AutocollimatorCalibrationSettings:
    calibration_factor: float
    zero_x: float
    zero_y: float

def calibration_plot(displacement, mirror, repeat, calibration_factor, ax1, ax2):
    """Plot the calibration points, the calibration line, and the difference between
    each calibration point and the calibration line"""
    for i in range(repeat):
        ax1.plot(mirror[i, :], displacement[i, :], 'o')

    sample = np.linspace(mirror.min() - 1.0, mirror.max() + 1.0, 100)
    ax1.plot(sample, calibration_factor * sample, '-')

    for i in range(repeat):
        autocollimator = displacement[i, :] / calibration_factor
        differences = autocollimator - mirror[i, :]
        ax2.plot(mirror[i, :], differences, 'o', label = 'Repeat {}'.format(i))

def axis_limit(ax1, ax2, direction):
    """Set the range of two subplot axes equal in the input direction"""
    if direction == 'x':
        min1, max1 = ax1.get_xlim()
        min2, max2 = ax2.get_xlim()
    elif direction == 'y':
        min1, max1 = ax1.get_ylim()
        min2, max2 = ax2.get_ylim()
    ax_min = np.min([min1, min2])
    ax_max = np.max([max1, max2])
    if direction == 'x':
        ax1.set_xlim(ax_min, ax_max)
        ax2.set_xlim(ax_min, ax_max)
    elif direction == 'y':
        ax1.set_ylim(ax_min, ax_max)
        ax2.set_ylim(ax_min, ax_max)

def yaw_and_pitch_calibration_plot(
    dfx_displacement, dfx_mirror, dfy_displacement, dfy_mirror,
    repeat, calibration_factor, calibration_time, args
    ):
    """Generate calibration report"""
    fig, ax = plt.subplots(2, 2, figsize = (16, 12), sharex = True)
    ax[0, 0].set_ylabel('Spot displacement (px)')
    ax[0, 1].set_ylabel('Spot displacement (px)')
    ax[1, 0].set_ylabel('Differences (arcsec)')
    ax[1, 1].set_ylabel('Differences (arcsec)')
    ax[1, 0].set_xlabel('Mirror yaw (arcsec)')
    ax[1, 1].set_xlabel('Mirror pitch (arcsec)')
    ax[1, 0].grid()
    ax[1, 1].grid()

    calibration_plot(
        dfx_displacement, dfx_mirror, repeat, calibration_factor, ax[0, 0], ax[1, 0]
        )
    calibration_plot(
        dfy_displacement, dfy_mirror, repeat, calibration_factor, ax[0, 1], ax[1, 1]
        )

    if repeat > 1.0:
        handles, labels = ax[1, 0].get_legend_handles_labels()
        plt.figlegend(
            handles, labels, fontsize = 14, loc = 'lower center',
            bbox_to_anchor = (0.5, 0.98), ncol = repeat, frameon = False
            )

    axis_limit(ax[0, 0], ax[0, 1], 'y')
    axis_limit(ax[1, 0], ax[1, 1], 'y')
    axis_limit(ax[1, 0], ax[1, 1], 'x')

    plt.tight_layout()
    plt.subplots_adjust(hspace = 0)
    with PdfPages(os.path.join(
        args.directory, 'Data', 'calibration_{}.pdf'.format(calibration_time)
        )) as pdf:
        pdf.savefig(fig, bbox_inches = 'tight')

def analyse_calibration_data(main_variable, secondary_variable, zero, repeat, resolution):
    """Return the pixel displacement and the mirror angle relative to the zero position
    defined by the calibration point closest to the corner cube zero"""
    number_of_points = np.shape(main_variable)[1]
    mirror = np.zeros([repeat, number_of_points])
    main_variable_zeroed = np.zeros([repeat, number_of_points])
    secondary_variable_zeroed = np.zeros([repeat, number_of_points])
    for i in range (repeat):
        zero_position = np.argmin(np.abs(main_variable[i, :] - zero))
        mirror[i, :] = (np.arange(number_of_points) - zero_position) * STEP_SIZE
        main_variable_zeroed[i, :] = main_variable[i, :] - main_variable[i, zero_position]
        secondary_variable_zeroed[i, :] = secondary_variable[i, :] - secondary_variable[i, zero_position]

    displacement = np.sign(main_variable_zeroed) * np.sqrt(main_variable_zeroed**2 + secondary_variable_zeroed**2)
    ind = (main_variable > resolution / 3.0) & (main_variable < 2.0 * resolution / 3.0)
    mirror_middle = mirror[ind]
    displacement_middle = displacement[ind]
    return mirror, mirror_middle, displacement, displacement_middle

def calibration(dfx, dfy, zero_x, zero_y, calibration_time, args, camera):
    """Calibrate pixel displacement against mirror angle through linear fitting"""
    print('Processing calibration data...')
    matplotlib.rcParams.update({'font.size': 16})

    repeat = np.shape(dfx)[0]

    for direction in ['x', 'y']:
        if direction == 'x':
            x = dfx[:, 0, :].copy()
            y = dfx[:, 1, :].copy()
            dfx_mirror, dfx_mirror_middle, dfx_displacement, dfx_displacement_middle = analyse_calibration_data(
                x, y, zero_x, repeat, camera.resolution.width
                )
        elif direction == 'y':
            x = dfy[:, 0, :].copy()
            y = dfy[:, 1, :].copy()
            dfy_mirror, dfy_mirror_middle, dfy_displacement, dfy_displacement_middle = analyse_calibration_data(
                y, x, zero_y, repeat, camera.resolution.height
                )

    calibration_x = np.polyfit(dfx_mirror_middle, dfx_displacement_middle, 1)
    calibration_y = np.polyfit(dfy_mirror_middle, dfy_displacement_middle, 1)
    calibration_factor = (calibration_x[0] + calibration_y[0]) / 2.0

    yaw_and_pitch_calibration_plot(
    dfx_displacement, dfx_mirror, dfy_displacement, dfy_mirror,
    repeat, calibration_factor, calibration_time, args
    )
    return AutocollimatorCalibrationSettings(calibration_factor, zero_x, zero_y)

def measure_zero_position(
    processed_outfile, raw_outfile, fraction, settings, overlay, args, camera
    ):
    """Measure the corner cube zero position"""
    save_data = False
    while not save_data:
    #We re-take the measurement until it's successful (i.e. the spot is bright enough)
        input("Place a corner cube in front of the mirror and press 'ENTER' to measure "
        'the zero position')
        frame, data, save_data = shared_functions.averaged_data_acquisition(
            fraction, overlay, camera
            )
    peak_mean, peak_std, maximum_pixel_value = shared_functions.averaged_data_processing(
        frame, data, settings, args, camera, 'pixel'
        )
    zero_x = peak_mean[0]
    zero_y = peak_mean[1]
    shared_functions.write_to_output_files(
        'Zero x position (px), Zero y position (px), Maximum pixel value\n',
        processed_outfile, raw_outfile
        )
    processed_outfile.write('{}, {}, {}\n'.format(*peak_mean, maximum_pixel_value))
    raw_outfile.write(str(data) + '\n')
    return zero_x, zero_y

def acquire_calibration_data(
    processed_outfile, raw_outfile, fraction, settings,
    overlay, args, camera, number_of_points = None
    ):
    """Collect and save calibration data"""
    calibration_data = []
    while True:
        inputstring = input()
        if inputstring == '':
            angle_arcsec = np.shape(calibration_data)[0] * STEP_SIZE
            frame, data, save_data = shared_functions.averaged_data_acquisition(
                fraction, overlay, camera
                )
            if not save_data:
                print("press 'ENTER' to re-take the measurement")
                if not number_of_points:
                    print("or input 'c' to finish the calibration run")
                continue
            peak_mean, peak_std, pixel_value_max = shared_functions.averaged_data_processing(
                frame, data, settings, args, camera, 'pixel'
                )
            calibration_data.append([*peak_mean])
            processed_outfile.write(
                '{}, {}, {}, {}\n'.format(angle_arcsec, *peak_mean, pixel_value_max)
                )
            raw_outfile.write(str(data) + '\n')
            print(
                'Rotate the hex key by 1/6 of a turn, '
                "then press 'ENTER' to take the next measurement"
                )
            if number_of_points:
                collected = np.shape(calibration_data)[0]
                print(
                    'Collected {} of {} data points so far'.format(collected, number_of_points)
                    )
                if collected >= number_of_points:
                    # If we've specified the number of points, we don't need to exceed this
                    calibration_data_arr = np.array(calibration_data).T
                    break
            else:
                print("Once this calibration run has been completed input 'c'")
        elif inputstring == 'c' and number_of_points == None:
            calibration_data_arr = np.array(calibration_data).T
            break
        elif inputstring == 'c' and number_of_points != None:
            print(
                'Repeated calibration runs must have the same number of calibration points\n'
                "Press 'ENTER' to take the next measurement"
                )
            continue
        else:
            print("Unrecognised input, press 'ENTER' to take the next measurement")
            continue
    return calibration_data_arr

def recalibrate_repeated(
    processed_outfile, raw_outfile, repeat, fraction, settings, overlay, args, camera
    ):
    """Repeat calibration data acquisition"""
    processed_outfile.write(
        'Angle (arcsec), Peak x position (px), Peak y position (px), Maximum pixel value\n'
        )
    raw_outfile.write('Peak x position (px), Peak y position (px), Maximum pixel value\n')
    m = int(repeat + 1)
    for i in range(m):
        if i == 0:
            calibration_data_arr = acquire_calibration_data(
                processed_outfile, raw_outfile, fraction, settings, overlay, args, camera
                )
            n = np.shape(calibration_data_arr)[-1]
            calibration_data = np.zeros([m, *np.shape(calibration_data_arr)])
            calibration_data[0] = calibration_data_arr
        if i > 0:
            spot_tracking.move_overlay(*calibration_data[0, :, 0], overlay, camera)
            shared_functions.write_to_output_files(
                'Repeat No. {}\n'.format(i),
                processed_outfile, raw_outfile, print_message = True
                )
            print(
                'Move the light spot back to the starting position indicated by the overlay\n'
                "then press 'ENTER' to take the first measurement"
                )
            calibration_data_arr = acquire_calibration_data(
                processed_outfile, raw_outfile, fraction, settings, overlay, args, camera, n
                )
            calibration_data[i] = calibration_data_arr
    return calibration_data

def recalibrate_yaw_and_pitch(
    processed_outfile, raw_outfile, zero_x, zero_y, repeat,
    fraction, settings, overlay, args, camera
    ):
    """Acquire yaw and pitch calibration data"""
    for direction in ['x', 'y']:
        if direction == 'x':
            print(
                '\nRemove the corner cube and move the light spot to the zero position '
                'indicated by the overlay\n'
                'While keeping the light spot visible, move it as far as possible to '
                'the left of the camera preview\n'
                "Press 'ENTER' to take the first measurement"
                )
            shared_functions.write_to_output_files(
                'Yaw calibration\n', processed_outfile, raw_outfile
                )
            dfx = recalibrate_repeated(
                processed_outfile, raw_outfile, repeat, fraction,
                settings, overlay, args, camera
                )
        elif direction == 'y':
            spot_tracking.move_overlay(zero_x, zero_y, overlay, camera)
            print('\nMove the light spot to the zero position indicated by the overlay\n'
            'While keeping the light spot visible, move it as far as possible to '
            'the top of the camera preview\n'
            "Press 'ENTER' to take the first measurement")
            shared_functions.write_to_output_files(
                'Pitch calibration\n', processed_outfile, raw_outfile
                )
            dfy = recalibrate_repeated(
                processed_outfile, raw_outfile, repeat, fraction,
                settings, overlay, args, camera
                )
    return dfx, dfy

def recalibrate(fraction, settings, args, camera):
    """Interactively recalibrate the autocollimator"""
    overlay = spot_tracking.create_overlay(camera)
    calibration_time = time.strftime("%Y%m%d_%H%M%S")
    repeat = input('Input the number of repeated calibration runs (0 for no repeat): ')
    repeat = int(repeat)
    working_distance = input(
        'Measure and input the shortest distance between '
        'the target mirror and the outer surface of the autocollimator in mm: '
        )
    working_distance = float(working_distance)
    with open(os.path.join(args.directory, 'Data', 'processed_calibration_data_{}.txt'.format(calibration_time)), 'w') as processed_outfile, \
        open(os.path.join(args.directory, 'Data', 'raw_calibration_data_{}.txt'.format(calibration_time)), 'w') as raw_outfile:
        shared_functions.write_to_output_files(
            (
                'Camera shutter speed (ms): {}\n'
                'Tracking threshold: {}\n'
                'Working distance (mm): {}\n'
                .format(
                    getattr(camera, 'shutter_speed'),
                    fraction,
                    working_distance
                    )
                ),
                processed_outfile, raw_outfile
            )
        zero_x, zero_y = measure_zero_position(
            processed_outfile, raw_outfile, fraction, settings, overlay, args, camera
            )
        dfx, dfy = recalibrate_yaw_and_pitch(
            processed_outfile, raw_outfile, zero_x, zero_y, repeat,
            fraction, settings, overlay, args, camera
            )
        calibration_settings = calibration(
            dfx, dfy, zero_x, zero_y, calibration_time, args, camera
            )
        save_calibration_settings(
            calibration_settings,
            os.path.join(args.directory, 'Settings', 'calibration_settings.json')
            )
        shared_functions.write_to_output_files(
            'Calibration factor (px/arcsec): {}'.format(calibration_settings.calibration_factor),
            processed_outfile, raw_outfile, print_message = True
            )
    camera.stop_preview()
    print(
        'The calibration report and the data files has been saved under the directory\n{}'
        .format(os.path.join(args.directory, 'Data'))
        )

def save_calibration_settings(settings, output):
    """Save calibration settings to a JSON file"""
    calibration_settings = {
        'Calibration factor (px/arcsec)': settings.calibration_factor,
        'Zero x position (px)': settings.zero_x,
        'Zero y position (px)': settings.zero_y
        }
    with open(output, 'w') as outfile:
        json.dump(calibration_settings, outfile, indent = 1)

def load_calibration_settings(inputf):
    """Load the calibration settings from a JSON file"""
    with open(inputf, 'r') as infile:
        settings = json.load(infile)
        calibration_factor = settings['Calibration factor (px/arcsec)']
        zero_x = settings['Zero x position (px)']
        zero_y = settings['Zero y position (px)']
    return AutocollimatorCalibrationSettings(calibration_factor, zero_x, zero_y)

def calculate_angle(x, y, settings):
    """Calculate angle using the calibration factor and the peak spot position"""
    zeroed_x = x - settings.zero_x
    zeroed_y = y - settings.zero_y
    yaw = zeroed_x / settings.calibration_factor
    pitch = zeroed_y / settings.calibration_factor
    return yaw, pitch

def measure(outfile, start_t, fraction, settings, overlay, args, camera, sensor):
    """Save measurement result in pixels and output to the terminal
    in either pixels or arcseconds.
    Print warning message if the signal is too weak or too intense"""
    frame, max_pixel_value = image_green(camera)
    t = time.time() - start_t
    if max_pixel_value == 0.0:
        outputstring = 'No signal detected'
    else:
        if 0.0 < max_pixel_value < 30.0:
            output_signal = '   SIGNAL IS TOO WEAK'
        elif 240.0 < max_pixel_value < 255.0:
            output_signal = '   SIGNAL IS TOO INTENSE'
        elif max_pixel_value == 255.0:
            output_signal = '   PIXEL SATURATION DETECTED'
        else:
            output_signal = ''

        peak = spot_tracking.image_peak(frame, fraction)[0]
        temp = sensor.temperature

        if args.pixel:
            output_result = 'Peak position (px): ({:.2f}, {:.2f})'.format(*peak)
        else:
            yaw, pitch = calculate_angle(peak[0], peak[1], settings)
            output_result = 'Measured angle (arcsec): ({:.2f}, {:.2f})'.format(yaw, pitch)

        if overlay != None:
            spot_tracking.move_overlay(*peak, overlay, camera)
        if outfile != None:
            outfile.write('{}, {}, {}, {}, {}\n'.format(t, *peak, max_pixel_value, temp))
        if args.save_frame:
            shared_functions.save_labelled_image(frame, args, camera)
    outputstring = 'Time (s): {:.0f}   '.format(t) + output_result + output_signal
    print('\r{0: <80}'.format(outputstring), end = '')

def continuous_measurement(fraction, settings, args, camera, sensor):
    """Loop the measure function until keyboard interupt"""
    overlay = shared_functions.preview(args, camera)
    if not args.save_data_off:
        outfile = open(os.path.join(
            args.directory,
            'Data',
            'autocollimator_drift_data_{}.txt'.format(time.strftime("%Y%m%d_%H%M%S"))
            ), 'w')
        outfile.write(
            'Camera shutter speed (ms): {}\n'
            'Tracking threshold: {}\n'
            .format(getattr(camera, 'shutter_speed'), fraction)
            )
        if settings != None:
            outfile.write(
                'Calibration factor (px/arcsec): {}\n'
                'Zero x position (px): {}\n'
                'Zero y position (px): {}\n'
                .format(settings.calibration_factor, settings.zero_x, settings.zero_y)
                )
        outfile.write(
            'Time (s), Peak x position (px), Peak y position (px), '
            'Maximum pixel value, Temperature (\u00b0C)\n'
            )
    elif args.save_data_off:
        outfile = None
    start_t = time.time()
    while True:
        measure(outfile, start_t, fraction, settings, overlay, args, camera, sensor)
        if shared_functions.input_nonblocking() == 'q':
            break
    shared_functions.terminate_continuous_measurement(outfile, args, camera)

def translation_stage(fraction, settings, args, camera):
    """Characterise the angular deviation of a linear translation stage"""
    overlay = shared_functions.preview(args, camera)
    characterisation_time = time.strftime("%Y%m%d_%H%M%S")
    working_distance_zero = input(
        'Adjust the position of the linear stage to zero\n'
        'Measure and input the shortest seperation between the mirror and '
        'the outer surface of the autocollimator in mm: '
        )
    working_distance_zero = float(working_distance_zero)
    travel_range = input('Input the travel range of the linear stage in mm: ')
    travel_range = int(travel_range)
    step = input('Input the measurement step size in mm: ')
    step = int(step)
    repeat = input('Input the number of repeated runs (0 for no repeat): ')
    repeat = int(repeat)
    with open(os.path.join(args.directory, 'Data', 'processed_translation_stage_characterisation_data_{}.txt'.format(characterisation_time)), 'w') as processed_outfile, \
        open(os.path.join(args.directory, 'Data', 'raw_translation_stage_characterisation_data_{}.txt'.format(characterisation_time)), 'w') as raw_outfile:
        shared_functions.write_to_output_files(
            (
                'This experiment characterises the angular deviation of '
                'a linear translation stage\n'
                'Camera shutter speed (ms): {}\n'
                'Tracking threshold: {}\n'
                'Calibration factor: {}\n'
                'Zero x position (px): {}\n'
                'Zero y position (px): {}\n'
                'Working distance when the linear stage is at zero (mm): {}\n'
                'Travel range of the linear stage (mm): {}\n'
                .format(
                    getattr(camera, 'shutter_speed'),
                    fraction,
                    settings.calibration_factor,
                    settings.zero_x,
                    settings.zero_y,
                    working_distance_zero,
                    travel_range
                    )
                ),
                processed_outfile, raw_outfile
            )
        shared_functions.acquire_linear_measurement(
            0, travel_range, step, repeat, processed_outfile, raw_outfile,
            fraction, settings, overlay, args, camera, 'angle'
            )
        if not args.camera_preview_off:
            camera.stop_preview()
        print(
            'The data files have been saved under the directory:\n{}'
            .format(os.path.join(args.directory, 'Data'))
            )

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = (
        'Autocollimator\n'
        'Running this program for the first time automatically starts '
        'the interactive calibration procedure\n'
        'Once calibrated, running this program with no arguments continuously output '
        'the measured angle to the terminal\n'
        "Continuous measurement can be terminated by pressing 'q' + 'ENTER'\n"
        ))
    shared_functions.addparser(parser)
    parser.add_argument(
        '-d', '--directory', help = 'path to the autocollimator directory',
        default = '/home/pi/autocollimator'
        )
    parser.add_argument(
        '-ts', '--translation_stage', action = 'store_true',
        help = 'Characterise the angular deviation of a linear translation stage'
        )
    args = parser.parse_args()

    with PiCamera(resolution = (1640, 1232), framerate = 40) as camera:
        i2c = busio.I2C(board.SCL, board.SDA)
        sensor = adafruit_sht31d.SHT31D(i2c)

        fraction, calibration_settings, args = shared_functions.startup(820, 616, args, camera)
        if args.reset_calibration:
            recalibrate(fraction, calibration_settings, args, camera)
        elif (not args.reset_calibration) & (args.translation_stage):
            translation_stage(fraction, calibration_settings, args, camera)
        else:
            continuous_measurement(fraction, calibration_settings, args, camera, sensor)
