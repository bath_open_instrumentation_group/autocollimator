# Assembly instructions

This folder contains the assembly instructions, in [gitbuilding] format.  This is a markdown file with embedded metadata, and can be best viewed [as a webpage](https://bath_open_instrumentation_group.gitlab.io/autocollimator/).  It will also partially render in GitLab's markdown [preview](./index.md), though this will lack the bill-of-materials information, and won't be formatted nicely.

## Compiling and editing

Please see the [gitbuilding] website for instructions.

[gitbuilding]: https://gitbuilding.io/