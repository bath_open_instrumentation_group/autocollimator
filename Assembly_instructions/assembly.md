# Assemble the autocollimator

{{BOM}}

The image below shows everything you need to build the ACute3D laser autocollimator.

![components](images/IMG_0333.JPG)



## Insert the achromatic lens {pagestep}
* Place the [achromatic lens]{qty: 1} on top of the [lens insertion tool][lens tool]{qty: 1} with the flat side facing up.
* Take the [autocollimator body][prepared body]{qty: 1} and push it down onto the lens. This will push-fit the lens into the triangular gripper. The triangular gripper is inside the vertical cuboid cutout of the autocollimator body (see the cutaway diagram on the index page).

![step1](images/IMG_0322.JPG)

[achromatic lens]: Parts.yaml#50mm_lens
[lens tool]: fromstep
[prepared body]: fromstep

## Attach the camera {pagestep}
Remove the original lens from the [Raspberry Pi camera]{qty: 1} and attach the camera module with the exposed imaging sensor to the [slip plate]{qty: 1} using 2 [M2x6 screws][M2x6 screw]{qty: 2}. [Thin shims][camera shim]{qty: 1, note: "You may need more than one"} might need to be added in between the camera module and the slip plate to ensure the imaging sensor is on the focal plane of the achromatic lens.

![step2](images/IMG_0321.JPG)

[Raspberry Pi camera]: Parts.yaml#camera
[M2x6 screw]: Parts.yaml#M2x6_screw
[slip plate]: fromstep
[camera shim]: fromstep

## Attach the slip plate {pagestep}
Attach the slip plate with the camera module to the autocollimator body and secure it in place using 4 [M3x16 screws][M3x16 screw]{qty: 4}. Thread a [spring washer]{qty: 4} and 2 [flat washers][flat washer]{qty: 8} onto each screw, the spring washer should be sandwiched in between the flat washers. The camera module should be oriented with the ribbon connector pointing down at the larger face of the autocollimator with only 4 holes. The slip plate allows 1 mm of motion about the screws for zero position adjustment.

![step3](images/IMG_0319.JPG)
![step4](images/IMG_0316.JPG)

[M3x16 screw]: Parts.yaml#M3x16_screw
[flat washer]: Parts.yaml#flat_washer
[spring washer]: Parts.yaml#spring_washer

## Connect the camera {pagestep}
Connect the [Raspberry Pi camera] to the [Raspberry Pi]{qty: 1} using a [camera ribbon cable]{qty: 1}.

![step5](images/IMG_0314.JPG)
![step6](images/IMG_0313.JPG)

[Raspberry Pi]: Parts.yaml#raspberry_pi_and_accessories
[camera ribbon cable]: Parts.yaml#camera_ribbon_cable

## Adjust the camera position {pagestep}
Point the camera at the far side of the room and use the [camera shims][camera shim] to get the far side of the room in focus.

* Turn on the Raspberry Pi, open up a console window and run:

```console
raspivid -t 0
```

This will display a full-screen preview of the camera image. You can close it by pressing `Control`+`C`.

* Assess whether the image is sharp, and whether it is improving or getting worse relative to the last setting.
* Shut down the Raspberry Pi and disconnect its power supply.
* Remove the camera from the slip plate.
* Add or remove a shim.
* Reattach the camera to the slip plate.
* Return to the start of these instructions.
* Once the far side of the room (or better a tree out the window) is in focus the imaging sensor is at the focal plane of the lens.

See the flowchart in the paper for more details.

>!! You should always shut down and turn off the Raspberry Pi before working on the camera module. Not doing so risks shorting the Pi and the camera module and potentially destroying both.

## Insert the microscope slide {pagestep}
Fully insert the [microscope slide]{qty: 1} at 45° into the slot on the side of the autocollimator body (highlighted in red in the image) and secure it in place using a [M3x12 Nylon screw]{qty: 1}. This is your beam splitter.

![step7](images/IMG_0310.JPG)
![step8](images/IMG_0308.JPG)

[microscope slide]: Parts.yaml#microscope_slide
[M3x12 Nylon screw]: Parts.yaml#M3x12_Nylon_screw

## Insert the laser diode {pagestep}
Insert the [laser diode](Parts.yaml#laser){qty: 1} into the irregular hole above the microscope slide slot (highlighted in red in the image) and secure it in place using 2 [M3x10 screws][M3x10 screw]{qty: 2}. This slot fixes the laser diode in place via three points contact when the screws are tightened. The laser diode should not extend into the vertical cuboid cutout in the autocollimator body as this will block the laser beam and cause unwanted reflections.

![step9](images/IMG_0307.JPG)
![step10](images/IMG_0305.JPG)

[M3x10 screw]: Parts.yaml#M3x10_screw

>! Consider using Nylon screws to avoid damaging the laser diode as a result of over tightening.

## Insert the ND filter {Pagestep}
Insert the [ND filter]{qty: 1} into the slot on one of the larger faces of the autocollimator (highlighted in red in the image) and secure it in place using the [printed lid]{qty: 1} and 4 [M3x6 screws][M3x6 screw]{qty: 4}. You need to thread a [flat washer]{qty: 4} onto each screw. The ND filter slot extends into the autocollimator body in a triangular shape, which fixes the ND filter in place via three points contact when the lid is attached. The three walls that the ND filter is in contact with are tilted to press the ND filter against the floor of the slot when the lid is tightened. Make sure the tip of the lid is inserted with the longer face on the top.

![step11](images/IMG_0302.JPG)
![step12](images/IMG_0295.JPG)
![step13](images/IMG_0293.JPG)

[ND filter]: Parts.yaml#ND_filter
[M3x6 screw]: Parts.yaml#M3x6_screw
[printed lid]: fromstep

## Attach the beam dump {Pagestep}
Attach the [beam dump]{qty: 1} to the autocollimator body using 4 [M3x6 screws][M3x6 screw]{qty: 4} to block the light reflected out of the vertical cuboid cutout in the autocollimator body.

![step14](images/IMG_0292.JPG)
![step15](images/IMG_0286.JPG)

[beam dump]: fromstep
