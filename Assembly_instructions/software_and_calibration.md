# Software and calibration

{{BOM}}

## Software setup {Pagestep}
In the Raspberry Pi file manager, create a folder named "autocollimator" under "home/pi". This is your autocollimator directory. Under the autocollimator directory create four folders named: "Code", "Settings", "Data", and "Frames". Copy and paste the [Python scripts] into the "Code" folder.

[Python scripts]: https://gitlab.com/bath_open_instrumentation_group/autocollimator/-/tree/master/Software

>! The "autocollimator" folder can be created at a different location in the file manager. If this is the case, you need to specify this directory using the -d argument when running the software. Alternatively, you can change the default autocollimator directory by editing the software.

## Fine camera position adjustment {Pagestep}
Project the laser beam at the centre of a [corner cube]{qty: 1} mounted on a [translation stage]{qty: 1}. Use the [camera shims][camera shim]{qty: 1, note: "You may need more than one"} and the [slip plate] to optimise the position of the imaging sensor. The facet of the corner cube should be slightly tilted to stop the unwanted surface reflection from entering the autocollimator.

![setup1](images/IMG_4709.JPG)

* Open up a console window on the Raspberry Pi and run:

```console
raspivid -t 0
```

* Adjust the slip plate position until the light spot on the camera is in the central region of the field of view.
* Close the preview by pressing `Control`+`C`. Navigate to the "Code" folder in the autocollimator directory and run:

```console
python3 autocollimator.py -p
```

* Measure the sensitivity of the spot position to beam translation by displacing the corner cube. Notice the laser beam is translated by 2 mm for 1 mm of corner cube displacement.
* Assess whether the sensitivity is lower compared to the last setting.
* Terminate the software by pressing `q`+`Enter`.
* Shut down the Raspberry Pi and disconnect its power supply.
* Remove the camera from the slip plate.
* Add or remove a shim.
* Reattach the camera to the slip plate.
* Return to the start of these instructions.
* Once the sensitivity to beam translation has minimized, the imaging sensor is at its optimal position.

See the flowchart in the paper for more details.

[corner cube]: Parts.yaml#corner_cube
[translation stage]: Parts.yaml#translation_stage
[slip plate]: fromstep
[camera shim]: fromstep

>!! You should always shut down and turn off the Raspberry Pi before working on the camera module. Not doing so risks shorting the Pi and the camera module and potentially destroying both.

## Calibrate the autocollimator {Pagestep}
The autocollimator software contains an interactive calibration procedure. This procedure prints step by step instructions to the console to guide the process. It assumes the calibration is performed by projecting the laser beam onto a plane mirror embedded inside a [ThorLabs Polaris-K1 mirror mount]{qty: 1}. During the calibration process, you need to adjust the angle of the mirror mount by rotating the control knob using a hex key into a mechanical stop. To use this interactive calibration procedure, open up a console window on the Raspberry Pi and navigate to the "Code" folder in the autocollimator directory. Then run:

```console
python3 autocollimator.py -rcs -rts -rc
```

![setup2](images/IMG_4717.JPG)

[ThorLabs Polaris-K1 mirror mount]: Parts.yaml#mirror_mount

>! The calibration can be performed using alternative mirror mounts. If this is the case, you need to specify the resolution of the mirror mount by modifying the software. As the calibration accuracy is directly dependent on the quality of the mirror mount, you should select the mirror mount carefully.
