# Printing
The ACute3D is intended to be printed on a RepRap-style fused filament fabrication [3D printer](Parts.yaml#3D_printer){cat: tool, qty: 1}, such as a Prusa i3 or an Ultimaker 2+. You will need about 200g of [PLA Filament](Parts.yaml#pla_filament){qty: "200g", note: "The diameter depends on your printer"}.

>i These parts do not require support, and enabling support may cause problems. They should print OK with or without brim.

## Print the parts {pagestep}
Print one copy of each of the files below, with the exception of the shim (see note).

* [monolithic_autocollimator.stl](../DesignFiles/STL/monolithic_autocollimator.stl){previewpage}
* [slip_plate.stl](../DesignFiles/STL/slip_plate.stl){previewpage}
* [lid.stl](../DesignFiles/STL/lid.stl){previewpage}
* [beamdump.stl](../DesignFiles/STL/beamdump.stl){previewpage}
* [lens_tool.stl](../DesignFiles/STL/lens_tool.stl){previewpage}
* [picamera_shim.stl](../DesignFiles/STL/picamera_shim.stl){previewpage} (you will probably need a few thicknesses which will involve modifying the OpenSCAD file)

>i You will most likely need more than one camera shim to position the imaging sensor on the focal plane of the lens. You may find it helpful to print several different thicknesses, by modifying the OpenSCAD file.

## Post print preparation {pagestep}
* Clean any stray strands of plastic off the printed parts.
* Prepare the autocollimator body: Drill the 11 holes highlighted in red in the image out to M2.5 and tap M3 threads into them.

![step1](images/IMG_0338.JPG)

You have now successfully made the [prepared body]{output, qty: 1}, [slip plate]{output, qty: 1}, [printed lid]{output, qty: 1}, [beam dump]{output, qty: 1}, [lens tool]{output, qty: 1}, and [camera shim]{output, qty: 2}.
