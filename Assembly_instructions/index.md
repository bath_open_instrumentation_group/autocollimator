# ACute3D

This folder contains instructions and the files to print and build the ACute3D, a laser autocollimator. The OpenSCAD code used to design the parts is also included. A schematic of the autocollimator is provided below.

![schematic](images/schematic.png)

* [.](printing.md){step}
* [.](assembly.md){step}
* [.](software_and_calibration.md){step}

The required items are listed in the [bill of materials]{BOM} page.