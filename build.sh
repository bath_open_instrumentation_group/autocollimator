#!/bin/bash
if [ ! -d STL ]; then
  mkdir STL;
fi

for file in $(ls STL/); do
  rm STL/$file;
done

cd OpenSCAD

for file in $(ls *.scad);do
  openscad -o ../STL/${file::-4}stl $file;
done
